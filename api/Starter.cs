using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Reactive.Disposables;
using System.IO;
using Microsoft.Extensions.Configuration;
using Autofac;

namespace api
{
	using core.commands;
	using core.logging;
	using core.io;
	using core.rx;
	using lines;

	public class Starter : IStartable, IDisposable
	{
		ILoggerAdapter logger;
		IOutputLines lines;
		ISerialReader reader;
		IDisposable toDispose;
		ICommandProcessor commandProcessor;
		IConfiguration config;
		ISerialWriter writer;

		public Starter(ILoggerAdapter<Starter> logger, IOutputLines lines, ISerialReader reader, ISerialWriter writer, ICommandProcessor commandProcessor, IConfiguration config)
		{
			this.logger = logger;
			this.lines = lines;
			this.reader = reader;
			this.commandProcessor = commandProcessor;
			this.config = config;
			this.writer = writer;
		}

		void IStartable.Start()
		{
			logger.LogDebug("starting up.");
			var readerSub = reader.Listen().Trace("All lines here.").Subscribe(lines.AddLine);
			writer.StartHandle();

			toDispose = Disposable.Create(() => {
				readerSub.Dispose();
			});

			ConfigureCommands();
		}

		void ConfigureCommands()
		{
			var raw = config.GetValue("CommandDirectory", "");
			if(string.IsNullOrWhiteSpace(raw))
				return;
			var split = raw.Split(";", StringSplitOptions.RemoveEmptyEntries);
			var files = new List<string>();
			foreach(var p in split)
			{
				if(!Directory.Exists(p))
					continue;
				files.AddRange(Directory.GetFiles(p));
			}
			HashSet<string> commandNames = new();
			foreach(var file in files)
			{
				var name = Path.GetFileName(file);
				name = name.Split('.')[0];
				if(commandNames.Contains(name))
					continue;
				commandNames.Add(name);
				this.commandProcessor.AddCommand(new serial.commands.DynamicCommand(name, () => WriteFile(file)));
			}
		}

		async Task<string> WriteFile(string path)
		{
			var lines = await File.ReadAllLinesAsync(path);
			foreach (var line in lines)
				await writer.Write(line);
			return "";
		}

		public void Dispose()
		{
			logger.LogDebug("shutting down.");
			toDispose.Dispose();
		}
	}
}
