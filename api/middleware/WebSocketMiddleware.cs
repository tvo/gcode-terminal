using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace api.middleware
{
	using core.logging;

	public class WebSocketMiddleware : IMiddleware
	{
		//public static const int GracefulClientTimout = 5;
		private readonly ILoggerAdapter logger;
		//private readonly ConcurrentDictionary<int, ConnectedClient> Clients = new ConcurrentDictionary<int, ConnectedClient>();

		// handle shutdown gracefully.
		//private readonly CancellationTokenSource SocketLoopTokenSource = new CancellationTokenSource();
		//private readonly CancellationTokenRegistration AppShutdownHandler;

		public WebSocketMiddleware(IHostApplicationLifetime hostLifetime, ILoggerAdapter<WebSocketMiddleware> logger)
        {
			this.logger = logger;
            //// gracefully close all websockets during shutdown (only register on first instantiation)
            //if(AppShutdownHandler.Token.Equals(CancellationToken.None))
                //AppShutdownHandler = hostLifetime.ApplicationStopping.Register(ApplicationShutdownHandler);
        }

		public async Task InvokeAsync(HttpContext context, RequestDelegate next)
		{
			await Task.CompletedTask;
			await next(context);
		}

		//private void Broadcast(string message, ILoggerAdapter logger)
        //{
            //logger.LogDebug($"Broadcasting: {message}");
            //foreach (var kvp in Clients)
                //kvp.Value.BroadcastQueue.Add(message);
        //}

		//private async Task CloseClientAsync(WebSocket client)
		//{
			//var socketState = client.State;
			//if (state == WebSocketState.Closed || state == WebSocketState.Aborted)
				//return;

			//var timeout = new CancellationTokenSource(TimeSpan.FromSeconds(TimeoutSeconds));
			//try
			//{
				//logger.LogTrace("... starting close handshake");
				//await client.Socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing", timeout.Token);
			//}
			//catch (OperationCanceledException ex)
			//{
				//Program.ReportException(ex);
				///
			//}
		//}

		//private async Task CloseAllSocketsAsync()
        //{
            //// We can't dispose the sockets until the processing loops are terminated,
            //// but terminating the loops will abort the sockets, preventing graceful closing.
            //var disposeQueue = new Queue<WebSocket>(Clients);

            //while (disposeQueue.Any())
            //{
                //var client = Clients.Dequeue();
                //logger.LogTrace($"WebSocketMiddleware: Closing Socket {client.SocketId}");

                //logger.LogTrace("WebSocketMiddleware: ending broadcast loop");
                //client.BroadcastLoopTokenSource.Cancel();

                //if (client.Socket.State != WebSocketState.Open)
                //{
                    //logger.LogTrace($"... socket not open, state = {client.Socket.State}");
                //}
                //else
                //{
                    //var timeout = new CancellationTokenSource(TimeSpan.FromSeconds(TimeoutSeconds));
                    //try
                    //{
                        //logger.LogTrace("... starting close handshake");
                        //await client.Socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing", timeout.Token);
                    //}
                    //catch (OperationCanceledException ex)
                    //{
                        //Program.ReportException(ex);
                        //// normal upon task/token cancellation, disregard
                    //}
                //}

                //if (Clients.TryRemove(client.SocketId, out _))
                //{
                    //// only safe to Dispose once, so only add it if this loop can't process it again
                    //disposeQueue.Add(client.Socket);
                //}
            //}

            //// now that they're all closed, terminate the blocking ReceiveAsync calls in the SocketProcessingLoop threads
            //SocketLoopTokenSource.Cancel();

            //// dispose all resources
            //foreach (var socket in disposeQueue)
                //socket.Dispose();
        //}
	}
}
