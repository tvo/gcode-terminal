using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args);
			host.Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
		{
            var host = Host.CreateDefaultBuilder(args);
			host.UseSerilog();
			host.UseServiceProviderFactory(new AutofacServiceProviderFactory());
            host.ConfigureWebHostDefaults(webBuilder =>
                {
					webBuilder.UseConfiguration(core.Configuration.BuildConfiguration());
					webBuilder.UseKestrel();
					webBuilder.UseContentRoot(Directory.GetCurrentDirectory());
                    webBuilder.UseStartup<Startup>();
                });
			return host;
		}
    }
}
