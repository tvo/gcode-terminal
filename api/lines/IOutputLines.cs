using System.Collections.Generic;

namespace api.lines
{
	public interface IOutputLines
	{
		void AddLine(string line);
		IEnumerable<(int i, string line)> GetLatest();
	}
}
