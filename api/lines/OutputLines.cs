using System.Collections.Concurrent;
using SlimMessageBus;

namespace api.lines
{
	public class OutputLines : IOutputLines, IConsumer<GCodeCompletedEvent>, IConsumer<LifeCycleEvent>
	{
		private ConcurrentQueue<(int i, string content)> Buffer;
		private const int bufferCount = 100;

		public OutputLines()
		{
			Buffer = new ConcurrentQueue<(int i, string content)>();
		}

		private readonly object CountLock = new Object();
		public void AddLine(string line)
		{
			lock(CountLock)
			{
				var max = 1;
				if(Buffer.Any())
					max = Buffer.Max(x => x.i) + 1;
				Buffer.Enqueue((max, line));
				if(Buffer.Count >= bufferCount)
					Buffer.TryDequeue(out var ignored);
			}
		}

		public Task OnHandle(GCodeCompletedEvent e)
		{
			AddLine(e.Line);
			return Task.CompletedTask;
		}

		public Task OnHandle(LifeCycleEvent e)
		{
			// Lifecycle event has to be caught, otherwise the publish will block the pipeline.
			return Task.CompletedTask;
		}

		public IEnumerable<(int i, string line)> GetLatest() => Buffer.ToList();
	}
}
