using System.Reflection;
using Serilog;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Autofac;
using SlimMessageBus.Host;

namespace api
{
	using lines;
	using core.logging;
	using core.webSockets;
	using core.io;
	using core.commands;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(x => x.InputFormatters.Insert(0, new Formatters.RawRequestBodyFormatter()));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "api", Version = "v1" });
            });
			services.AddSlimMessageBus(mbb =>
			{
			  mbb.AddServicesFromAssembly(Assembly.GetExecutingAssembly());
			  mbb.AddServicesFromAssembly(Assembly.GetAssembly(typeof(core.CoreModule).GetType()));
			});
        }

		public void ConfigureContainer(ContainerBuilder builder)
		{
			builder.RegisterModule(new LoggerModule(Configuration));
			builder.RegisterModule(new WebSocketModule(Configuration));
			builder.RegisterModule(new IOModule(Configuration));
			builder.RegisterModule(new CommandModule(Configuration));

			builder.RegisterType<Starter>().AutoActivate().AsImplementedInterfaces();
			builder.RegisterType<OutputLines>().AsImplementedInterfaces().SingleInstance();
		}

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
			else
				app.UseHttpsRedirection();

			app.UseSwagger();
			app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "api v1"));
			app.UseSerilogRequestLogging();

            app.UseRouting();
			app.UseWebSockets(new WebSocketOptions{KeepAliveInterval = TimeSpan.FromSeconds(120)});

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
