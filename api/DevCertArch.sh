#!/bin/bash

if [[ "$1" != "" ]]; then
    CERTNAME="$1"
else
    CERTNAME=cert
fi

openssl req -x509 -newkey rsa:4096 -keyout ${CERTNAME}_key.pem -out $CERTNAME.pem -days 365
openssl pkcs12 -export -out $CERTNAME.pfx -inkey ${CERTNAME}_key.pem -in $CERTNAME.pem
openssl pkcs12 -in $CERTNAME.pfx -clcerts -nokeys -out $CERTNAME.crt
