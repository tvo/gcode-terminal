using System;
using System.Linq;
using System.Threading.Tasks;
using System.Reactive.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

using api.lines;

namespace api.Controllers
{
	using core.logging;
	using core.pubSub;
	using core.webSockets;
	using core.io;
	using core.commands;

    [ApiController]
    [Route("[controller]")]
	public class MicroController : ControllerBase
	{
		ILoggerAdapter logger;
		ICommandProcessor processor;
		ISerialWriter writer;

		public MicroController(ILoggerAdapter<CommandController> logger,
								 ICommandProcessor processor,
								 ISerialWriter writer)
		{
			this.logger = logger;
			this.processor = processor;
			this.writer = writer;
		}

		[HttpPost("connect")]
		public IActionResult Connect([FromServices] IPortIO portManager)
		{
			portManager.Open();
			return Ok();
		}

		[HttpPost("close")]
		public IActionResult Close([FromServices] IPortIO portManager)
		{
			portManager.Close();
			return Ok();
		}

		[HttpGet("console")]
		//[HttpConnect("console")] // https://learn.microsoft.com/en-us/aspnet/core/fundamentals/websockets?view=aspnetcore-7.0 . Does HttpConnect exist yet?
		public async Task<IActionResult> ConsoleOut([FromServices] ISerialReader reader, [FromServices] IOutputLines lines, 
													[FromServices] Subject<SocketMessage> socketMessages, [FromServices] SocketConnection connection)
		{
			var latest = lines.GetLatest().ToDictionary(x => x.i, x => x.line);

			if(HttpContext.WebSockets.IsWebSocketRequest)
			{
				using var webSocket = await HttpContext.WebSockets.AcceptWebSocketAsync(new WebSocketAcceptContext { DangerousEnableCompression = true });
				logger.LogDebug("WebSocket connection established, {state}", webSocket.State);

				using var _ = socketMessages.Add(m => logger.LogWarning("Websocket message from client: {message}", m.Message));
				using var sub = reader.Listen()
					.SelectMany(async x => {
						logger.LogDebug("About to publish message from the websocket {message}", x);
						await socketMessages.Publish(new SocketMessage(x));
						return true;
					})
					.Subscribe();

				await connection.Listen(webSocket);
				logger.LogDebug("Closing websocket");
				return new EmptyResult();
			}
			else
				return Ok(latest);
		}
	}
}
