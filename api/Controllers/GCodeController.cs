using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using core.logging;
using core.io;
using core.commands;

namespace api.Controllers
{
    [ApiController]
    [Route("[controller]")]
	public class GCodeController : ControllerBase
	{
		ILoggerAdapter logger;
		ICommandProcessor processor;
		ISerialWriter writer;

		public GCodeController(ILoggerAdapter<CommandController> logger,
								 ICommandProcessor processor,
								 ISerialWriter writer)
		{
			this.logger = logger;
			this.processor = processor;
			this.writer = writer;
		}

		[HttpPost("Abort")]
		public IActionResult Abort([FromServices] IPortIO portManager)
		{
			portManager.Clear();
			logger.LogWarning("Aborted current process queue.");
			return Ok();
		}

		[HttpPost]
		public IActionResult RunMany([FromBody] string content)
		{
			var eol = new []{"\r\n", "\r", "\n"};
			foreach(var g in content.Split(eol, StringSplitOptions.RemoveEmptyEntries))
				writer.Write(g);
			return Ok();
		}
	}
}
