using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using core.logging;
using core.io;

namespace api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Route("/")]
	public class VersionController : ControllerBase
	{
		public VersionController()
		{
		}

		[HttpGet]
		public string version()
		{
			return "v1.0";
		}
	}
}
