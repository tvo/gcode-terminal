using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

using core.logging;
using core.io;

using api.lines;

namespace api.Controllers
{
    [ApiController]
    [Route("[controller]")]
	public class StatusController : ControllerBase
	{
		private readonly ILoggerAdapter logger;

		public StatusController(ILoggerAdapter<CommandController> logger)
		{
			this.logger = logger;
		}

		[HttpGet("port")]
		public string port([FromServices] IConfiguration config)
		{
			return config.GetSection("com").GetValue<string>("port");
		}

		[HttpGet("position")]
		public string position()
		{
			return "xyz";
		}

	}
}
