using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using core.logging;
using core.io;
using core.commands;

namespace api.Controllers
{
    [ApiController]
    [Route("[controller]")]
	public class CommandController : ControllerBase
	{
		ILoggerAdapter logger;
		ICommandProcessor processor;
		ISerialWriter writer;

		public CommandController(ILoggerAdapter<CommandController> logger,
								 ICommandProcessor processor,
								 ISerialWriter writer)
		{
			this.logger = logger;
			this.processor = processor;
			this.writer = writer;
		}

		[HttpGet]
		public IActionResult GetCommands()
		{
			return Ok(processor.AllCommands.Keys);
		}

		[HttpPost]
		public async Task<IActionResult> Run([FromBody] string command)
		{
			return Ok(await processor.ProcessCommand(command));
		}
	}
}
