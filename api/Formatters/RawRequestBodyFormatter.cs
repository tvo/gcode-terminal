using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Net.Http.Headers;

namespace api.Formatters
{
	public class RawRequestBodyFormatter : InputFormatter
	{
		private List<string> MediaTypes = new []{ "text/plain", "application/octet-stream" }.ToList();
		private bool CanReadMediaType (string contentType) => MediaTypes.Any(x => contentType.Contains(x));

		public RawRequestBodyFormatter()
		{
			foreach (var t in MediaTypes)
				SupportedMediaTypes.Add(new MediaTypeHeaderValue(t));
		}

		public override bool CanRead(InputFormatterContext context)
		{
			if (context == null)
				throw new ArgumentNullException(nameof(context));

			return CanReadMediaType(context.HttpContext.Request.ContentType);
		}

		public override async Task<InputFormatterResult> ReadRequestBodyAsync(InputFormatterContext context)
		{
			var request = context.HttpContext.Request;
			var contentType = context.HttpContext.Request.ContentType;

			if (string.IsNullOrEmpty(contentType) || contentType.Contains("text/plain"))
			{
				using var reader = new StreamReader(request.Body);
				var content = await reader.ReadToEndAsync();
				return await InputFormatterResult.SuccessAsync(content);
			}
			else if (contentType.Contains("application/octet-stream"))
			{
				using var ms = new MemoryStream(2048);
				await request.Body.CopyToAsync(ms);
				var content = ms.ToArray();
				return await InputFormatterResult.SuccessAsync(content);
			}

			return await InputFormatterResult.FailureAsync();
		}
	}
}

