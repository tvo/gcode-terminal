using Xunit;
using Moq;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;

namespace test.PortStuff
{
	using core.io;

	public class SerialWriterScalerTests
	{
		private ISerialWriter concrete = Mock.Of<ISerialWriter>();
		private Mock<ISerialWriter> concreteMock => Mock.Get(concrete);

		private SerialWriterScaler decorator;
		private List<string> WriteData = new List<string>();


		public SerialWriterScalerTests()
		{
			concreteMock.Setup(x => x.Write(It.IsAny<string>()))
				.Callback<string>(cb => WriteData.Add(cb));

			decorator = new SerialWriterScaler(concrete);
		}

		[Fact]
		public void Write_CallsBase()
		{
			// Given
			var line = "G1 X1 Y1 Z1 F100.91"; // 100.91 does not parse perfectly into a double.

			// When
			decorator.Write(line);

			// Then
			concreteMock.Verify(x => x.Write(line), Times.Once);
		}

		[Fact]
		public void Write_WithSpeed_ThenWithout_SpeedIsAdded()
		{
			// Given
			decorator.SetSpeed(2);

			// When
			decorator.Write("G1 X1 Y1 Z1 F10");
			decorator.Write("G0 X1 Z100");

			// Then
			foreach(var sample in WriteData)
			{
				core.gcode.GCodeParser.TryParseMove(sample, out var x2, out var y2, out var z2, out var updatedFeed);
				updatedFeed.Should().Be(20);
			}
		}

		[Fact]
		public void Write_NoMultiplierAdjustment_FeedThenNoFeed()
		{
			// Given - no speed adjustment
			// When
			decorator.Write("G1 X1 Y1 Z1 F10");
			decorator.Write("G0 X1 Z100");

			// Then
			WriteData[0].Should().Be("G1 X1 Y1 Z1 F10");
			WriteData[1].Should().Be("G0 X1 Z100");
		}

		[Theory]
		[InlineData("G0 X1 Y2 Z3 F10")]
		[InlineData("G0 X0.01 Y0.02 Z0.03 F1")]
		[InlineData("G1 X0.01 Y0.02 Z0.03 F1")]
		[InlineData("G1 X1 Y1 Z1 F0.5")]
		public void Write_DoubleFeed(string line)
		{
			// Given
			core.gcode.GCodeParser.TryParseMove(line, out var x, out var y, out var z, out var oldFeed);
			decorator.SetSpeed(2);

			// When
			decorator.Write(line);

			// Then
			line = WriteData.Single();
			core.gcode.GCodeParser.TryParseMove(line, out var x2, out var y2, out var z2, out var updatedFeed);
			(oldFeed * 2f).Should().Be(updatedFeed);
		}

		[Theory]
		[InlineData("G0 X1 Y2 Z3 F10")]
		[InlineData("G0 X0.01 Y0.02 Z0.03 F1")]
		[InlineData("G1 X0.01 Y0.02 Z0.03 F1")]
		[InlineData("G1 X1 Y1 Z1 F0.5")]
		public void Write_HalfFeed(string line)
		{
			// Given
			core.gcode.GCodeParser.TryParseMove(line, out var x, out var y, out var z, out var oldFeed);
			decorator.SetSpeed(0.5);

			// When
			decorator.Write(line);

			// Then
			line = WriteData.Single();
			core.gcode.GCodeParser.TryParseMove(line, out var x2, out var y2, out var z2, out var updatedFeed);
			(oldFeed / 2f).Should().Be(updatedFeed);
		}
	}
}
