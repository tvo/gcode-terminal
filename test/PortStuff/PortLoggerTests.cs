using System;
using Xunit;
using Moq;

namespace test.PortStuff
{
	using core.logging;
	using core.io;

	public class PortLoggerTests
	{
		private ILoggerAdapter<PortLogger> Logger;
		private IPortIO dep;
		private PortLogger port;

		public PortLoggerTests()
		{
			Logger = Mock.Of<ILoggerAdapter<PortLogger>>();
			var loggerMock = Mock.Get(Logger);
			loggerMock.Setup(x => x.LogDebug(It.IsAny<string>()));

			dep = Mock.Of<IPortIO>();
			var depMock = Mock.Get(dep);
			depMock.Setup(x => x.Close());
			depMock.Setup(x => x.Open());
			depMock.Setup(x => x.Listen());
			depMock.Setup(x => x.Write(It.IsAny<string>()));

			port = new PortLogger(dep, Logger);
		}

		[Fact]
		public void WriteCallsLogger()
		{
			// Given
			var asdf = "text";

			// When
			port.Write(asdf);

			// Then
			var loggerMock = Mock.Get(Logger);
			loggerMock.Verify(x => x.LogDebug(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
		}

		[Fact]
		public void WriteCallsBase()
		{
			// Given
			var asdf = "text";

			// When
			port.Write(asdf);

			// Then
			Mock.Get(dep).Verify(x => x.Write(asdf), Times.Once);
		}

		[Fact]
		public void OpenCallsLogger()
		{
			// Given
			// When
			port.Open();

			// Then
			var loggerMock = Mock.Get(Logger);
			loggerMock.Verify(x => x.LogDebug(It.IsAny<string>()), Times.Once);
		}

		[Fact]
		public void OpenCallsBase()
		{
			// Given
			// When
			port.Open();

			// Then
			Mock.Get(dep).Verify(x => x.Open(), Times.Once);
		}

		[Fact]
		public void CloseCallsLogger()
		{
			// Given
			// When
			port.Close();

			// Then
			var loggerMock = Mock.Get(Logger);
			loggerMock.Verify(x => x.LogDebug(It.IsAny<string>()), Times.Once);
		}

		[Fact]
		public void CloseCallsBase()
		{
			// Given
			// When
			port.Close();

			// Then
			Mock.Get(dep).Verify(x => x.Close(), Times.Once);
		}

		[Fact]
		public void Clear_CallsBase_LogsMessage()
		{
			// Given
			// When
			port.Clear();

			// Then
			Mock.Get(dep).Verify(x => x.Clear(), Times.Once);
			Mock.Get(Logger).Verify(x => x.LogInformation(It.IsAny<string>()), Times.Once);
		}
	}
}
