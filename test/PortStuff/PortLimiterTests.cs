using Xunit;
using Moq;
using System.Linq;
using System.Reactive.Subjects;
using System.Reactive.Linq;

namespace test.PortStuff
{
	using core.io;
	using core.logging;

	public class PortLimiterTests
	{
		private IPortIO dep;
		private PortLimiter port;

		Subject<string> ListenData = new Subject<string>();

		public PortLimiterTests()
		{
			dep = Mock.Of<IPortIO>();
			var depMock = Mock.Get(dep);
			depMock.Setup(x => x.Listen()).Returns(ListenData);
			port = new PortLimiter(dep);
		}

		[Fact]
		public void WriteCallsBase()
		{
			// Given
			var asdf = "text";

			// When
			port.Write(asdf);

			// Then
			Mock.Get(dep).Verify(x => x.Write(asdf), Times.Once);
		}

		[Fact]
		public void OpenCallsBase()
		{
			// Given
			// When
			port.Open();

			// Then
			Mock.Get(dep).Verify(x => x.Open(), Times.Once);
		}

		[Fact]
		public void CloseCallsBase()
		{
			// Given
			// When
			port.Close();

			// Then
			Mock.Get(dep).Verify(x => x.Close(), Times.Once);
		}

		[Fact]
		public void ListenCallsBase()
		{
			// Given
			var depM = Mock.Get(dep);
			depM.Setup(x => x.Listen()).Returns(Observable.Never<string>());

			// When
			port.Listen();

			// Then
			Mock.Get(dep).Verify(x => x.Listen(), Times.Once);
		}

		[Fact]
		public void WriteManyTimes_NoOk_OnlyCallsWriteOnce()
		{
			// Given
			var depM = Mock.Get(dep);
			depM.Setup(x => x.Listen()).Returns(Observable.Never<string>());

			// When
			foreach(var i in Enumerable.Range(1, 10))
				port.Write(i.ToString());

			// Then
			Mock.Get(dep).Verify(x => x.Write("1"), Times.Once);
		}

		[Fact]
		public void WriteManyTimes_SeveralOk_WriteIsCalledCorrectNumberOfTimes()
		{
			// Given
			var toWrite = Enumerable.Range(1, 20).Select(x => x.ToString());
			var ok = Enumerable.Range(1, 8).Select(x => x.ToString());

			// When
			foreach(var i in toWrite)
				port.Write(i);
			foreach(var i in ok)
				ListenData.OnNext("ok");

			// Then
			foreach(var i in Enumerable.Range(1, 9).Select(x => x.ToString()))
				Mock.Get(dep).Verify(x => x.Write(It.Is<string>(x => x.Equals(i))), Times.Once());
			foreach(var i in Enumerable.Range(10, 10).Select(x => x.ToString()))
				Mock.Get(dep).Verify(x => x.Write(It.Is<string>(x => x.Equals(i))), Times.Never());
		}

		[Fact]
		public void Clear_DoesNotThrow()
		{
			port.Clear();
		}

		[Fact]
		public void WriteWrite_Clear_Ok_WritesOnlyOnce()
		{
			// Given
			port.Write("good");
			port.Write("bad");

			// When
			port.Clear();
			ListenData.OnNext("ok"); // ok should clear up the following write, if it wasn't cleared.

			// Then
			Mock.Get(dep).Verify(x => x.Write("bad"), Times.Never);
		}

		[Fact]
		public void Write_Clear_Write_Ok_WritesTwice()
		{
			// Given
			port.Write("good");

			// When
			port.Clear();
			port.Write("good");
			ListenData.OnNext("ok"); // ok should clear up the following write, if it wasn't cleared.

			// Then
			Mock.Get(dep).Verify(x => x.Write("good"), Times.Exactly(2));
		}
	}
}
