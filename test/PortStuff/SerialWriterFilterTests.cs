using Xunit;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace test.PortStuff
{
	using core.io;

	public class SerialWriterFilterTests
	{
		private ISerialWriter concrete = Mock.Of<ISerialWriter>();
		private Mock<ISerialWriter> concreteMock => Mock.Get(concrete);

		private SerialWriterFilter decorator;
		private List<string> WriteData = new List<string>();


		public SerialWriterFilterTests()
		{
			concreteMock.Setup(x => x.Write(It.IsAny<string>()))
				.Callback<string>(cb => WriteData.Add(cb));

			decorator = new SerialWriterFilter(concrete);
		}

		[Fact]
		public async Task Write_CallsBase()
		{
			// Given
			var line = "G1 X1 Y1 Z1 F100.91";

			// When
			await decorator.Write(line);

			// Then
			concreteMock.Verify(x => x.Write(line), Times.Once);
		}

		[Theory]
		[InlineData((string)null)]
		[InlineData("")]
		[InlineData(" ")]
		[InlineData("     ")] // spaces
		[InlineData("			")] // tabs
		public async Task Write_FiltersNullEmptyWhitespace(string line)
		{
			// Given
			// When
			await decorator.Write(line);

			// Then
			concreteMock.Verify(x => x.Write(It.IsAny<string>()), Times.Never);
		}

		[Theory]
		[InlineData("(comment) G0 X1", "G0 X1")]
		[InlineData("G0 X1 (comment)", "G0 X1")]
		[InlineData("G0 X1;comment", "G0 X1")]
		[InlineData("G0 X1 ;comment", "G0 X1")]
		public async Task Write_RemovesComments(string line, string match)
		{
			// Given
			// When
			await decorator.Write(line);

			// Then
			concreteMock.Verify(x => x.Write(match), Times.Once);
		}

		[Theory]
		[InlineData("(comment)")]
		[InlineData(" ;comment")]
		[InlineData("(comment ) ;comment")]
		[InlineData(";comment then gcode G0 X1")]
		public async Task Write_RemovesCommentsAndWholeLine(string line)
		{
			// Given
			// When
			await decorator.Write(line);

			// Then
			concreteMock.Verify(x => x.Write(It.IsAny<string>()), Times.Never);
		}
	}
}
