using Xunit;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Reactive.Subjects;
using System.Reactive.Linq;

namespace test.PortStuff
{
	using core;
	using core.io;
	using core.io.slimLock;

	public class SerialWriterTests
	{
		private IPortIO port;
		private ISlimLock slimLock;

		private SerialWriter writer;

		Subject<string> ListenData = new Subject<string>();

		public SerialWriterTests()
		{
			port = Mock.Of<IPortIO>();
			var portM = Mock.Get(port);
			slimLock = Mock.Of<ISlimLock>();

			writer = new SerialWriter(port, slimLock);
		}

		[Fact]
		public void Startup_LockInitallyBlocks()
		{
			// Given
			var lockM = Mock.Get(this.slimLock);

			// When - constructor already called.
			// Then
			lockM.Verify(x => x.WaitAsync(), Times.Once);
			lockM.Verify(x => x.Release(), Times.Never);
			lockM.Verify(x => x.Dispose(), Times.Never);
		}

		[Fact]
		public async Task StartUp_CallsRelease_NotLockedAgain()
		{
			// Given
			var lockM = Mock.Get(this.slimLock);
			writer.StartHandle();

			// When
			lockM.Verify(x => x.WaitAsync(), Times.Once); // initial wait
			lockM.Verify(x => x.Release(), Times.Once); // initial release after startup.
			await writer.Write("asdf");

			// Then
			lockM.Verify(x => x.Release(), Times.Exactly(2));
			// could be waitasync with or without cancellation token.
			lockM.Verify(x => x.WaitAsync(default), Times.Exactly(1)); // uses wait to aquire the lock.
		}

		[Fact]
		public async Task WriteMany_CallBase_OnStartUp()
		{
			// Given
			var portM = Mock.Get(port);
			writer.StartHandle();
			foreach(var _ in Enumerable.Range(1, 19))
				await writer.Write("ASDF");

			// When
			writer.StartHandle();

			// Then
			portM.Verify(x => x.Write("ASDF"), Times.Exactly(19));
		}

		[Fact]
		public async Task WriteMany_Uppercase()
		{
			// Given
			var portM = Mock.Get(port);
			writer.StartHandle();

			// When
			await writer.Write("asdf");

			// Then
			portM.Verify(x => x.Write("ASDF"), Times.Once);
		}
	}
}
