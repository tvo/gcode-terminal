using System;
using System.Reflection;
using Xunit;
using serial;

namespace test
{

    /// Add these two tests to your test project. One of them looks for immutable structs, the other looks for classes.
    public class ImmutabilityTest
    {
        /// This test verifies immutable structs
        [Fact]
        public void EnsureStructsAreImmutableTest()
        {
            Type t = typeof(Position);
            var p = new Assembly[] { t.Assembly };
            new Immutability.Test(p).EnsureStructsAreImmutableTest();
        }

        /// This test verifies the immutability of classes.
        [Fact]
        public void EnsureImmutableTypeFieldsAreMarkedImmutableTest()
        {
            Type t = typeof(Position);
            var p = new Assembly[] { t.Assembly };
            new Immutability.Test(p).EnsureImmutableTypeFieldsAreMarkedImmutableTest();
        }
    }

}
