using Xunit;
using serial;
using FluentAssertions;

namespace test.library
{
	public class PositionTests
	{
		[Fact]
		public void CtorTest()
		{
			// Given
			Position p;
			// When
			p = new Position(1, 2, 3);
			// Then
			p.X.Should().Be(1);
			p.Y.Should().Be(2);
			p.Z.Should().Be(3);
		}

		[Fact]
		public void FromTest()
		{
			// Given
			Position p;
			// When
			p = Position.From(4);
			// Then
			p.X.Should().Be(4);
			p.Y.Should().Be(4);
			p.Z.Should().Be(4);
		}

		[Fact]
		public void Add_Positions()
		{
			// Given
			var left = new Position(1,2,3);
			var right = new Position(10, 20, 30);
			// When
			var p = left + right;
			// Then
			p.X.Should().Be(11);
			p.Y.Should().Be(22);
			p.Z.Should().Be(33);
		}

		[Fact]
		public void Subtract_Positions()
		{
			// Given
			var left = new Position(11, 22, 33);
			var right = new Position(1,2,3);
			// When
			var p = left - right;
			// Then
			p.X.Should().Be(10);
			p.Y.Should().Be(20);
			p.Z.Should().Be(30);
		}

		[Fact]
		public void Multiply_Positions()
		{
			// Given
			var left = new Position(10, 100, 1000);
			var right = new Position(2,3,4);
			// When
			var p = left * right;
			// Then
			p.X.Should().Be(20);
			p.Y.Should().Be(300);
			p.Z.Should().Be(4000);
		}

		[Fact]
		public void Divide_Positions()
		{
			// Given
			var left = new Position(10, 200, 3000);
			var right = new Position(10,100,1000);
			// When
			var p = left / right;
			// Then
			p.X.Should().Be(1);
			p.Y.Should().Be(2);
			p.Z.Should().Be(3);
		}

		[Fact]
		public void RelativeMove_Nulls()
		{
			// Given
			var left = new Position(10, 200, 3000);
			// When
			var p = left.RelativeMove(null, null, null);
			// Then
			p.X.Should().Be(10);
			p.Y.Should().Be(200);
			p.Z.Should().Be(3000);
		}

		[Fact]
		public void RelativeMove_Move()
		{
			// Given
			var left = new Position(10, 200, 3000);
			// When
			var p = left.RelativeMove(1, 2, 3);
			// Then
			p.X.Should().Be(11);
			p.Y.Should().Be(202);
			p.Z.Should().Be(3003);
		}

		[Fact]
		public void AbsoluteMove_Nulls()
		{
			// Given
			var left = new Position(10, 200, 3000);
			// When
			var p = left.AbsoluteMove(null, null, null);
			// Then
			p.X.Should().Be(10);
			p.Y.Should().Be(200);
			p.Z.Should().Be(3000);
		}

		[Fact]
		public void AbsoluteMove_Move()
		{
			// Given
			var left = new Position(10, 200, 3000);
			// When
			var p = left.AbsoluteMove(1, 2, 3);
			// Then
			p.X.Should().Be(1);
			p.Y.Should().Be(2);
			p.Z.Should().Be(3);
		}
	}
}
