using Xunit;
using FluentAssertions;

namespace test.library
{
	using core.gcode;

	public class GCodeParserTests
	{
		[Fact]
		public void Empty()
		{
			// Given
			// When
			var can = GCodeParser.TryParseMove(string.Empty, out var x, out var y, out var z);

			// Then
			can.Should().BeFalse();
		}

		[Fact]
		public void NullString()
		{
			// Given
			// When
			var can = GCodeParser.TryParseMove(null, out var x, out var y, out var z);

			// Then
			can.Should().BeFalse();
		}

		[Fact]
		public void GOne_XYZ()
		{
			// Given
			// When
			var can = GCodeParser.TryParseMove("G1 X1 Y2 Z3", out var x, out var y, out var z);

			// Then
			can.Should().BeTrue();
			x.Should().Be(1);
			y.Should().Be(2);
			z.Should().Be(3);
		}

		[Fact]
		public void GZero_XYZ()
		{
			// Given
			// When
			var can = GCodeParser.TryParseMove("G0 X1 Y2 Z3", out var x, out var y, out var z);

			// Then
			can.Should().BeTrue();
			x.Should().Be(1);
			y.Should().Be(2);
			z.Should().Be(3);
		}

		[Fact]
		public void GZero_X()
		{
			// Given
			// When
			var can = GCodeParser.TryParseMove("G0 X1", out var x, out var y, out var z);

			// Then
			can.Should().BeTrue();
			x.Should().Be(1);
			y.Should().Be(null);
			z.Should().Be(null);
		}

		[Fact]
		public void GZero_Y()
		{
			// Given
			// When
			var can = GCodeParser.TryParseMove("G0 Y1", out var x, out var y, out var z);

			// Then
			can.Should().BeTrue();
			x.Should().Be(null);
			y.Should().Be(1);
			z.Should().Be(null);
		}

		[Fact]
		public void GZero_Z()
		{
			// Given
			// When
			var can = GCodeParser.TryParseMove("G0 Z1", out var x, out var y, out var z);

			// Then
			can.Should().BeTrue();
			x.Should().Be(null);
			y.Should().Be(null);
			z.Should().Be(1);
		}

		[Fact]
		public void GZero_Z_FeedRate()
		{
			// Given
			// When
			var can = GCodeParser.TryParseMove("G0 Z1 F200", out var x, out var y, out var z, out var feed);

			// Then
			can.Should().BeTrue();
			x.Should().Be(null);
			y.Should().Be(null);
			z.Should().Be(1);
			feed.Should().Be(200);
		}

		[Fact]
		public void GZero_AllIntegers()
		{
			// Given
			// When
			var can = GCodeParser.TryParseMove("G0 x1 y2 Z3 F4", out var x, out var y, out var z, out var feed);

			// Then
			can.Should().BeTrue();
			x.Should().Be(1);
			y.Should().Be(2);
			z.Should().Be(3);
			feed.Should().Be(4);
		}

		[Fact]
		public void GZero_AllDoubles()
		{
			// Given
			// When
			var can = GCodeParser.TryParseMove("G0 x1.1 y2.2 Z3.3 F4.4", out var x, out var y, out var z, out var feed);

			// Then
			can.Should().BeTrue();
			x.Should().Be(1.1f);
			y.Should().Be(2.2f);
			z.Should().Be(3.3f);
			feed.Should().Be(4.4f);
		}

		[Fact]
		public void GZero_LargeNumbers()
		{
			// Given
			// When
			var can = GCodeParser.TryParseMove("G0 x1000 y2000 Z3000 F4000", out var x, out var y, out var z, out var feed);

			// Then
			can.Should().BeTrue();
			x.Should().Be(1000f);
			y.Should().Be(2000f);
			z.Should().Be(3000f);
			feed.Should().Be(4000f);
		}

		[Fact]
		public void GZero_SmallNumbers()
		{
			// Given
			// When
			var can = GCodeParser.TryParseMove("G0 x0.001 y0.002 Z0.003 F0.004", out var x, out var y, out var z, out var feed);

			// Then
			can.Should().BeTrue();
			x.Should().Be(0.001f);
			y.Should().Be(0.002f);
			z.Should().Be(0.003f);
			feed.Should().Be(0.004f);
		}
	}
}
