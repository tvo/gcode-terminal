using Xunit;
using FluentAssertions;
using System;
using System.Threading.Tasks;
using System.Reactive.Linq;

namespace test.library
{
	using core.rx;

	public class BroadcastTest
	{
		[Fact]
		public async Task one()
		{
			// Given
			var source = Observable.Return(1).Broadcast();
			var a = 0;

			// When
			var subA = source.Do(v => {
				a = v;
			});
			var disA = source.Subscribe();
			await subA;
			disA.Dispose();

			// Then
			a.Should().Be(1);
		}


		//[Fact]
		//public async Task one_sourceDoesNotComplete()
		//{
			//// Given

			////var source = Observable.Interval(TimeSpan.FromSeconds(2));
			//var source = Observable.Create<int>(x => {
				//x.OnNext(1);
				//return Disposable.Empty;
			//});
			//source = source.Broadcast();

			//var a = 0;
			//// When
			////source = source.ObserveOn(NewThreadScheduler.Default);
			////source =  source.SubscribeOn(NewThreadScheduler.Default);

			//source = source.Select(v => {
				//a = 1;
				//return 100;
			//});

			//await Task.Delay(3000);
			//var disA = source.Subscribe(s => {
			//});

			//var t = Task.Run(() => {
				//System.Threading.Thread.Sleep(6000);
				//Console.WriteLine("dispose");
				//disA.Dispose();
			//});

			//await Task.Delay(3000);
			//Console.WriteLine("await");

			//await Task.Delay(3000);

			//// Then
			//a.Should().Be(1);
		//}

		[Fact]
		public async Task Two_SetupFirst()
		{
			// Given
			var source = Observable.Return(1).Broadcast();
			var a = 0;
			var b = 0;

			// When
			var subA = source.Do(v => a = v);
			var subB = source.Do(v => b = v);
			var disA = source.Subscribe();
			var disB = source.Subscribe();
			await subA;
			await subB;
			disA.Dispose();
			disB.Dispose();

			// Then
			a.Should().Be(1);
			b.Should().Be(1);
		}
	}
}
