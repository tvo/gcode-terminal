using Xunit;
using FluentAssertions;
using System.Linq;
using api.lines;

namespace test.api
{
	public class OutputLineTests
	{
		[Fact]
		public void OutputLines_Init_IsEmpty()
		{
			// Given
			var lines = new OutputLines();

			// When - nothing
			var output = lines.GetLatest();

			// Then
			output.Should().BeEmpty();
		}

		[Fact]
		public void OutputLines_AddLine_ReturnsOne()
		{
			// Given
			var lines = new OutputLines();
			lines.AddLine("asdf");

			// When - nothing
			var output = lines.GetLatest();

			// Then
			output.Should().HaveCount(1);
			var item = output.First();
			item.i.Should().Be(1);
			item.line.Should().Be("asdf");
		}

		[Fact]
		public void OutputLines_AddLines_CorrectOrder()
		{
			// Given
			var lines = new OutputLines();
			lines.AddLine("asdf");
			lines.AddLine("zxcv");

			// When - nothing
			var output = lines.GetLatest();

			// Then
			output.Should().HaveCount(2);
			var first = output.First();
			first.i.Should().Be(1);
			first.line.Should().Be("asdf");
			var last = output.Last();
			last.i.Should().Be(2);
			last.line.Should().Be("zxcv");
		}
	}
}
