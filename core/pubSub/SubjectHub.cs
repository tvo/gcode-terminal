using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Reactive.Disposables;

namespace core.pubSub
{
	public class SubjectHub<T>
	{
		private readonly ConcurrentDictionary<(object sender, object instance), Func<T, Task>> handles = new ConcurrentDictionary<(object sender, object instance), Func<T, Task>>(); 

		public async Task Publish(T message, Subject<T> sender)
		{
			var everybodyElse = handles
				.Where(x => x.Key.sender != sender)
				.Select(x => x.Value);
			foreach(var handle in everybodyElse)
				await handle(message);
		}

		public IDisposable Add(Action<T> handle, Subject<T> sender)
		{
			return Add(x => {
				handle(x);
				return Task.CompletedTask;
			}, sender);
		}
		public IDisposable Add(Func<T, Task> handle, Subject<T> sender)
		{
			var key = (sender, new Object());
			handles.TryAdd(key, handle);
			return Disposable.Create(() => handles.TryRemove(key, out var _));
		}
	}
}
