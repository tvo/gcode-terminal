using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Reactive.Disposables;

namespace core.pubSub
{
	public class Subject<T>
	{
		private readonly SubjectHub<T> Hub;

		public Subject(SubjectHub<T> hub)
		{
			Hub = hub;
		}

		public Task Publish(T message) => Hub.Publish(message, this);

		public IDisposable Add(Func<T, Task> handle) => Hub.Add(handle, this);
		public IDisposable Add(Action<T> handle) => Hub.Add(handle, this);
	}
}
