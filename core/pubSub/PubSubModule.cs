using System;
using Autofac;
using Microsoft.Extensions.Configuration;

namespace core.pubSub
{
	using rx;

	public class PubSubModule : Module
	{
		private readonly IConfiguration config;

		public PubSubModule(IConfiguration config)
		{
			this.config = config;
		}

		protected override void Load(ContainerBuilder builder)
		{
			// this may be overkill for just a module...
			builder.RegisterGeneric(typeof(Subject<>)).AsSelf().InstancePerDependency();
			builder.RegisterGeneric(typeof(SubjectHub<>)).AsSelf().InstancePerLifetimeScope(); // singleton or lifetime scope?
			// need to call the subjectHub at the higest scope if calling from multiple scope levels.
		}
	}
}
