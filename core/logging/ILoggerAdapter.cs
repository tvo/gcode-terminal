namespace core.logging
{
	[Immutability.Immutable]
	public interface ILoggerAdapter
	{
		void LogTrace(string message);
		void LogDebug(string message);
		void LogInformation(string message);
		void LogWarning(string message);
		void LogError(string message);

		void LogTrace(string message, params object[] args);
		void LogDebug(string message, params object[] args);
		void LogInformation(string message, params object[] args);
		void LogWarning(string message, params object[] args);
		void LogError(string message, params object[] args);
	}

	[Immutability.Immutable]
	public interface ILoggerAdapter<T> : ILoggerAdapter
	{
	}
}
