using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace core.io
{
	using core;
	using core.io.slimLock;
	using core.logging;

	public class SerialWriterLock : ISerialWriter, IDisposable
	{
		private readonly ISerialWriter instance;
		private readonly ISlimLock writeLock;

		public SerialWriterLock(ISerialWriter instance, ISlimLock slim)
		{
			this.instance = instance;
			writeLock = slim;
			writeLock.WaitAsync().Wait(); // this should always complete immediately as it was just created.
		}

		public void Dispose()
		{
			writeLock.Dispose();
		}

		public void StartHandle()
		{
			writeLock?.Release();
			instance.StartHandle();
		}

		public Task Write(string data)
		{
			return writeLock.WaitAsync(() => instance.Write(data));
		}

		public Task Write(IEnumerable<string> data)
		{
			return writeLock.WaitAsync(() => instance.Write(data));
		}
	}
}
