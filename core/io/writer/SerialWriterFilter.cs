using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace core.io
{
	public class SerialWriterFilter : ISerialWriter
	{
		private readonly ISerialWriter Instance;

		public SerialWriterFilter(ISerialWriter instance)
		{
			Instance = instance;
		}

		private static Regex CommentParens = new Regex(@"\(.*\)", RegexOptions.Compiled);
		private static Regex CommentSemiColin = new Regex(@";.*", RegexOptions.Compiled);

		public void StartHandle() => Instance.StartHandle();

		public IEnumerable<string> Filter(IEnumerable<string> data)
		{
			foreach(var line in data)
			{
				if(string.IsNullOrWhiteSpace(line) || line.StartsWith(";"))
					continue;
				var result = line;
				if(CommentParens.IsMatch(result))
					result = CommentParens.Replace(result, "");
				if(CommentSemiColin.IsMatch(result))
					result = CommentSemiColin.Replace(result, "");

				result = result.Trim();
				if(string.IsNullOrEmpty(result))
					continue;
				yield return result;
			}
		}

		public async Task Write(string data)
        {
			foreach(var line in Filter(new []{data}))
				await Instance.Write(line);
        }

        public async Task Write(IEnumerable<string> data)
        {
			data = Filter(data);
			await Instance.Write(data);
        }
    }
}
