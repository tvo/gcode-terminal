using System.Collections.Generic;
using System.Threading.Tasks;

namespace core.io
{
	public interface ISerialWriter
	{
		Task Write(string data);
		Task Write(IEnumerable<string> data);
		void StartHandle();
	}
}
