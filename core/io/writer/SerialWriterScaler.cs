using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace core.io
{
	public class SerialWriterScaler : ISerialWriter
	{
		private readonly ISerialWriter Instance;
		private double Multiplier = 1.0;
		private double? LastFeedRate = null;

		public SerialWriterScaler(ISerialWriter instance)
		{
			Instance = instance;
		}

		public void SetSpeed(double multiplier)
		{
			Multiplier = multiplier;
		}

		private static Regex CommentParens = new Regex(@"\(.*\)", RegexOptions.Compiled);
		private static Regex CommentSemiColin = new Regex(@";.*", RegexOptions.Compiled);

		public void StartHandle() => Instance.StartHandle();

		private bool IsWithin(double number, double compare, double margin) => number + margin > compare && number - margin < compare;


		private string ScaleCode(string data)
		{
			if(!IsWithin(1.0, Multiplier, 0.005) && core.gcode.GCodeParser.TryParseMove(data, out var x, out var y, out var z, out var feed))
			{
				var sb = new StringBuilder(data.Substring(0,2));
				if(feed != null)
					LastFeedRate = feed;
				if(LastFeedRate != null)
				{
					if(x != null)
						sb.Append($" X{x}");
					if(y != null)
						sb.Append($" Y{y}");
					if(z != null)
						sb.Append($" Z{z}");
					sb.Append($" F{LastFeedRate.Value * Multiplier}");

					data = sb.ToString().Trim();
				}
			}
			return data;
		}


        public Task Write(string data)
        {
			return Instance.Write(ScaleCode(data));
        }

        public Task Write(IEnumerable<string> data)
        {
			return Instance.Write(data.Select(x => ScaleCode(x)));
        }
    }
}
