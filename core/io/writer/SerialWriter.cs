using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace core.io
{
	using core;
	using core.io.slimLock;

	public class SerialWriter : ISerialWriter, IDisposable
	{
		private readonly Action<string> Writer;
		private readonly ISlimLock writeLock; // this write lock is locking writes until the startup happens.

		public SerialWriter(IPortIO sender, ISlimLock slimLock)
		{
			Writer = sender.Write;
			writeLock = slimLock;
			writeLock.WaitAsync().Wait(); // this should always complete immediately as it was just created.
		}

		public void Dispose()
		{
			writeLock.Dispose();
		}

		public void StartHandle() => writeLock?.Release();

		public Task Write(string data)
		{
			return writeLock.WaitAsync(() => Writer(data.ToUpper()));
		}

		public Task Write(IEnumerable<string> data)
		{
			return writeLock.WaitAsync(() => {
				foreach(var line in data)
					Writer(line.ToUpper());
			});
		}
	}
}
