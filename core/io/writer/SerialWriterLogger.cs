using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace core.io
{
	using logging;
	using rx;

	public class SerialWriterLogger : ISerialWriter
	{
		private readonly ISerialWriter Instance;
		private readonly ILoggerAdapter Logger;

		public SerialWriterLogger(ISerialWriter instance, ILoggerAdapter<SerialWriterLogger> logger)
		{
			Instance = instance;
			Logger = logger;
		}

		public void StartHandle()
		{
			Logger.LogDebug("Writer starting. Emptying write queue.");
			Instance.StartHandle();
		}

        public Task Write(string data)
        {
			Logger.LogDebug("Serial Writer, writing: {data}", data);
			return Instance.Write(data);
        }

		public Task Write(IEnumerable<string> data)
        {
			//Logger.LogDebug("Serial Writer, writing many lines");
			data = data.Select(x => {
				Logger.LogDebug("Serial Writer, writing line: {data}", x);
				return x;
			});
			return Instance.Write(data);
        }
    }
}
