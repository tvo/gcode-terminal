using System;

namespace core.io
{
	public interface ISerialReader
	{
		IObservable<string> Listen();
		bool Started();
	}
}
