using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace core.io
{
	using rx;
	
	public class SerialReader : ISerialReader
	{
		private readonly IObservable<string> listener;
		private readonly BehaviorSubject<bool> startup;

		public SerialReader(IPortIO sender)
		{
			startup = new (false);
			listener = sender.Listen().Do(line => EvaluateStartup(line));
		}

		public void EvaluateStartup(string text)
		{
			if(startup.Value)
				return;
			startup.OnNext(true);
		}

		public IObservable<string> Listen() => listener;
		public bool Started() => startup.Value;
	}
}
