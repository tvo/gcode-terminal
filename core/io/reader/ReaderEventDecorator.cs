using System;
using System.Reactive.Linq;
using SlimMessageBus;

namespace core.io
{
	public class ReaderEventDecorator : ISerialReader
	{
		private readonly ISerialReader instance;
		private readonly IMessageBus bus;

		public ReaderEventDecorator(ISerialReader instance, IMessageBus bus)
		{
			this.instance = instance;
			this.bus = bus;
		}

		private void PublishGcode(string line) => bus.Publish(new events.GCodeCompletedEvent(line)).Wait();
		private bool prevStarted = false;
		private void PublishStartSequence(string line)
		{
			if(instance.Started() && !prevStarted)
			{
				bus.Publish(new events.LifeCycleEvent("system-startup")).Wait();
				prevStarted = true;
			}
		}

		public IObservable<string> Listen() => instance.Listen().Do(PublishGcode).Do(PublishStartSequence);
		public bool Started() => instance.Started();
	}
}
