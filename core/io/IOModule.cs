using Autofac;
using Microsoft.Extensions.Configuration;

namespace core.io
{
	using slimLock;

	public class IOModule : Module
	{
		private readonly IConfiguration config;

		public IOModule(IConfiguration config)
		{
			this.config = config;
		}

		protected override void Load(ContainerBuilder builder)
		{
			var portSettings = new PortSettings();
			config.GetSection("com").Bind(portSettings);
			builder.RegisterInstance(portSettings);


			if(!config.GetValue<bool>("useServer", false))
			{
				builder.RegisterDecorator<PortLogger, IPortIO>();
				builder.RegisterDecorator<PortLimiter, IPortIO>();
				if(portSettings.Fake)
					builder.RegisterType<FakePort>().As<IPortIO>().InstancePerLifetimeScope().SingleInstance();
				else
					builder.RegisterType<PortManager>().As<IPortIO>().InstancePerLifetimeScope().SingleInstance();

				builder.RegisterType<SerialWriter>().As<ISerialWriter>().SingleInstance();
				builder.RegisterType<SerialReader>().As<ISerialReader>();
			}

			builder.RegisterType<SlimLock>().As<ISlimLock>();
			
			builder.RegisterDecorator<ReaderEventDecorator, ISerialReader>();

			builder.RegisterDecorator<SerialWriterLock, ISerialWriter>();
			builder.RegisterDecorator<SerialWriterScaler, ISerialWriter>();
			builder.RegisterDecorator<SerialWriterLogger, ISerialWriter>();
			builder.RegisterDecorator<SerialWriterFilter, ISerialWriter>();
		}
	}
}
