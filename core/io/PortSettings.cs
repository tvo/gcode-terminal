namespace core.io
{
	public class PortSettings
	{
		public bool Fake { get; set; } = false;
		public string Port { get; set; } = "/dev/ttyUSB0";
		public int Baud { get; set; } = 115200;
	}
}
