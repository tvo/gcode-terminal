using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Reactive.Disposables;

namespace core.io
{
	using core.rx;

	// this could be named better in the future potentially.
	/// This class can observe gcode, and will send up to the buffered amount to the cnc.
	public class PortLimiter : IPortIO, IDisposable
	{
		private readonly Queue<string> input = new Queue<string>();
		private readonly Queue<string> working = new Queue<string>();
		private readonly IPortIO Instance;
		private readonly CompositeDisposable soonTrash = new CompositeDisposable();
		private readonly Subject<string> _ListenSubject = new Subject<string>();

		public PortLimiter(IPortIO instance)
		{
			Instance = instance;

			var listen = Instance.Listen()
				.Do(CheckForOk) // ok goes to the input queue.
				.Where(x => x != "ok") // filter ok
				;
			soonTrash.Add(listen.Subscribe(_ListenSubject));
		}

		private void _AddToWorking()
		{
			var data = input.Dequeue();
			working.Enqueue(data);
			Instance.Write(data);
		}

		private void CheckForOk(string data)
		{
			data = data?.ToLower() ?? "";
			if(data != "ok" || working.Count == 0)
				return;

			_ListenSubject.OnNext($"{data}: {working.Dequeue()}");
			
			// more work?
			if(input.Count > 0)
				_AddToWorking();
		}

        public IObservable<string> Listen() => _ListenSubject;

        public void Write(string data)
        {
			input.Enqueue(data);
			// handle next stage.
			if(working.Count == 0)
				_AddToWorking();
        }

		public void Clear()
		{
			input.Clear();
			Instance.Clear();
		}

        public void Open() => Instance.Open();
        public void Close() => Instance.Close();

        public void Dispose()
        {
			soonTrash.Dispose();
        }
    }
}
