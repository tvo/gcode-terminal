﻿using System;
using System.IO.Ports;
using System.Reactive.Linq;
using System.Reactive.Concurrency;

namespace core.io
{
	using rx;

	// this class converts the dotnet "Port" class into an observable / observer.
	public class PortManager : IPortIO
	{
		private SerialPort Port = null;
		private readonly PortSettings Settings;
		private readonly IObservable<string> Listener;

		public PortManager(PortSettings settings)
		{
			Settings = settings;
			Open();

			Listener = ReadEvent().Broadcast();
		}

		private IObservable<string> ReadEvent()
		{
			return Observable.FromEventPattern<SerialDataReceivedEventHandler, SerialDataReceivedEventArgs>(
					h => Port.DataReceived += h,
					h => Port.DataReceived -= h)
				.Select(x => 1)
				// I don't trust the event.
				.Merge(Observable.Interval(TimeSpan.FromSeconds(1)).Select(x => 1))

				.ObserveOn(ThreadPoolScheduler.Instance)
				.SubscribeOn(ThreadPoolScheduler.Instance)
				.Select(ignored => Port.ReadLine());
		}

		public IObservable<string> Listen() => Listener;

		public void Write(string data)
		{
			// do you need a lock here? maybe not...
			if(!Port?.IsOpen == true)
				throw new ApplicationException("Port is not open yet");
			Port.WriteLine(data);
		}

		public void Clear()
		{
			// nothing to do here I think...
		}

		public void Open()
		{
			if(Port?.IsOpen == true)// no need to re-open an open port.
				return;

			// assign
			if(Port is null)
				Port = new SerialPort(Settings.Port, Settings.Baud);

			Port.Open(); // this does the stuff.
		}

		public void Close()
		{
			if(Port is null)
				return;
			if(!Port.IsOpen)
				return;
			Port.Close();
		}
	}
}
