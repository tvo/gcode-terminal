using System;

namespace core.io
{
	public interface IPortIO
	{
		IObservable<string> Listen();
		void Write(string data);
		void Clear();
		void Open();
		void Close();
	}
}
