using System;
using System.Threading.Tasks;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace core.io
{
	using core.rx;

	public class FakePort : IPortIO
	{
		private readonly ISubject<string> ListenData = new ReplaySubject<string>();
		public IObservable<string> Listen()
		{
			TimeSpan sec(double number) => TimeSpan.FromSeconds(number);

			return ListenData
				.Merge(SendIn("start", sec(0)))
				.Merge(SendIn("echo: this is a bootup message", sec(0.25)))
				.Merge(SendIn("another message", sec(1)))
				.Merge(SendIn("free memory", sec(1.5)))
				.Broadcast()
				;
		}

		public IObservable<string> SendIn(string message, TimeSpan fromNow)
		{
			return Observable.FromAsync(async () => {
				await Task.Delay(fromNow);
				return message;
			});
		}

		public IObserver<string> Writer()
		{
			return Observer.Create<string>(data => {
				ListenData.OnNext("ok");
			}, error => {}, () => {});
		}

		public void Open() { }
		public void Close(){ }
		public void Clear(){ }

		public void Write(string data)
		{
			Task.Delay(200).Wait();
			ListenData.OnNext("ok");
		}
	}
}
