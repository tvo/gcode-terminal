using System;

namespace core.io
{
	using rx;
	using logging;

    public class PortLogger : IPortIO
	{
		private readonly IPortIO Instance;
		private readonly ILoggerAdapter Logger;
		private readonly Lazy<IObservable<string>> SharedListen;

		public PortLogger(IPortIO instance, ILoggerAdapter<PortLogger> logger)
		{
			Instance = instance;
			Logger = logger;
			SharedListen = new Lazy<IObservable<string>>(() => Instance
					.Listen()
					.Trace(Logger, "portInput")
					.Broadcast()
				);
		}

		public void Clear()
		{
			Instance.Clear();
			Logger.LogInformation("Port buffer cleared");
		}

		public void Open()
		{
			Logger.LogDebug("Opening port.");
			Instance.Open();
		}

		public void Close()
		{
			Logger.LogDebug("Closing port.");
			Instance.Close();
		}

        public IObservable<string> Listen() => SharedListen.Value;

		public void Write(string data) // this is needed for the observable writer.
		{
			Logger.LogDebug("Writing to port: {data}", data);
			Instance.Write(data);
		}
    }
}
