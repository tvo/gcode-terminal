using System;
using System.Threading;
using System.Threading.Tasks;

namespace core.io.slimLock
{
	public interface ISlimLock : IDisposable
	{
		Task WaitAsync();
		Task WaitAsync(CancellationToken token);
		void Release();
	}
}
