using System;
using System.Threading;
using System.Threading.Tasks;

namespace core.io.slimLock
{
	public class SlimLock: ISlimLock
	{
		private readonly SemaphoreSlim slim;

		public SlimLock()
		{
			slim = new SemaphoreSlim(1);
		}

		public Task WaitAsync() => slim.WaitAsync();
		public Task WaitAsync(CancellationToken token) => slim.WaitAsync(token);
		public void Release() => slim.Release();
		public void Dispose() => slim.Dispose();
	}
}
