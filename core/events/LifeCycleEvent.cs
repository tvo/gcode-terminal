namespace core.events;

public record LifeCycleEvent(string Name);
