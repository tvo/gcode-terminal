namespace core.events;

public record GCodeWriteEvent(string Line);
public record GCodeCompletedEvent(string Line);
