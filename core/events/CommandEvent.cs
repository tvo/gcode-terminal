namespace core.events;

public record CommandEvent(string Name);