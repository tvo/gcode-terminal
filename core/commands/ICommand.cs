using System;
using System.Threading.Tasks;

namespace core.commands
{
	public interface ICommand
	{
		Task<string> DoCommand(string[] args);
		string Name { get; }
	}
}
