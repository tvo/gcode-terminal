using Autofac;
using Microsoft.Extensions.Configuration;

namespace core.commands
{
	public class CommandModule : Module
	{
		private readonly IConfiguration config;

		public CommandModule(IConfiguration config)
		{
			this.config = config;
		}

		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterDecorator<CommandProcessorLogger, ICommandProcessor>();
			builder.RegisterType<CommandProcessor>().As<ICommandProcessor>().SingleInstance();
		}
	}
}

