using System.Threading.Tasks;
using System.Collections.Immutable;
using System.Linq;
using System.Reactive.Linq;

namespace core.commands
{
	using core.logging;

	public class CommandProcessorLogger : ICommandProcessor
	{
		private readonly ILoggerAdapter Logger;
		private readonly ICommandProcessor Instance;

		public CommandProcessorLogger(ICommandProcessor instance, ILoggerAdapter<CommandProcessor> log)
		{
			Logger = log;
			Instance = instance;
		}

        public ImmutableDictionary<string, ICommand> AllCommands => Instance.AllCommands;
        public async Task<string> ProcessCommand(string commandText)
		{
			Logger.LogInformation("Running: {command}", commandText);
			var result = await Instance.ProcessCommand(commandText);
			if(result == "The command is not found.")
				Logger.LogWarning("Bad command: {result}", result);
			else
				Logger.LogDebug("command result: {result}", result);
			return result;
		}

		public void AddCommand(ICommand command)
		{
			Logger.LogDebug("Adding command to list {command}", command.Name);
			Instance.AddCommand(command);
		}
    }
}
