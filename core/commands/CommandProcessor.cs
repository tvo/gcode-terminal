using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reactive.Subjects;

namespace core.commands
{
	public class CommandProcessor : ICommandProcessor
	{
		private readonly Subject<string> statusSubject = new Subject<string>();

		public ImmutableDictionary<string, ICommand> AllCommands { get; set; }

		public CommandProcessor(IEnumerable<ICommand> commands)
		{
			AllCommands = commands.ToImmutableDictionary(x => x.Name, x => x);
		}

		public void AddCommand(ICommand command)
		{
			AllCommands = AllCommands.SetItem(command.Name, command);
		}

		public async Task<string> ProcessCommand(string commandText)
		{
			// since this can be changed with AddCommand. A local variable keeps this consistent.
			var allCommands = AllCommands;

			var args = commandText.Split(" ");
			if(args.Length == 0)
				return "";
			var arg0 = args[0];
			if(!allCommands.ContainsKey(arg0))
				return "The command is not found.";
			var command = allCommands[arg0];
			return (await command.DoCommand(args.Skip(1).ToArray())) ?? ":" + commandText;
		}
	}
}
