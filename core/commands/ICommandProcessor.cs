using System.Threading.Tasks;
using System.Collections.Immutable;

namespace core.commands
{
	public interface ICommandProcessor
	{
		ImmutableDictionary<string, ICommand> AllCommands { get; }
		Task<string> ProcessCommand(string commandText);
		void AddCommand(ICommand command);
	}
}
