using System;
using System.Threading.Tasks;

namespace serial.commands
{
	using core.commands;

	[Immutability.Immutable]
	public class DynamicCommand : ICommand
	{
		public string Name { get; }

		private readonly Func<Task<string>> _handler;

		public DynamicCommand(string name, Func<Task<string>> handler)
		{
			Name = name;
			_handler = handler;
		}

		public bool IsMe(string arg) => arg.StartsWith(Name + " ");
		public async Task<string> DoCommand(string[] args)
		{
			return await _handler();
		}
	}
}
