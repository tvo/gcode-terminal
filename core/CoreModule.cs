using Microsoft.Extensions.Configuration;
using Autofac;

namespace core;

// todo - is this useful? If not remove.
public class CoreModule : Module
{
    private readonly IConfiguration config;

    public CoreModule(IConfiguration config)
    {
        this.config = config;
    }

    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterModule(new logging.LoggerModule(config));
    }
}