using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace core.rx
{
	using logging;

	public static class ObservableTrace
	{
		public class TraceObservable<T> : IObservable<T>
		{
			private readonly string Name;
			private readonly ILoggerAdapter Logger;
			private readonly IObservable<T> Source;

			public TraceObservable(IObservable<T> source, int id, ILoggerAdapter logger) : this(source, id.ToString(), logger)
			{
			}

			public TraceObservable(IObservable<T> source, string name, ILoggerAdapter logger)
			{
				Name = name;
				Logger = logger;
				Source = source;
			}

			private void LogNext(T value)
			{
				Logger?.LogDebug("OnNext: {id}, value: {value}", Name, value);
			}
			private void LogError(Exception e)
			{
				Logger?.LogError("OnError: {id}, value: {error}", Name, e);
			}
			private void LogCompleted()
			{
				Logger?.LogDebug("OnCompleted: {id}", Name);
			}
			private void LogDisposed()
			{
				Logger?.LogDebug("OnDisposed: {id}", Name);
			}

			public IDisposable Subscribe(IObserver<T> dest)
			{
				void next(T value)
				{
					LogNext(value);
					dest.OnNext(value);
				}
				void error(Exception e)
				{
					LogError(e);
					dest.OnError(e);
				}
				void complete()
				{
					LogCompleted();
					dest.OnCompleted();
				}

				IDisposable toDispose = Source.Subscribe(next, error, complete);
				return Disposable.Create(() => {
					LogDisposed();
					toDispose.Dispose();
				});
			}
		}

		private static Lazy<ILoggerAdapter> _logger = new Lazy<ILoggerAdapter>(() => null); // this will be set by the configure method.
		private static int GlobalId = 0;

		public static void ConfigureLogger(ILoggerAdapter logger) => ConfigureLogger(new Lazy<ILoggerAdapter>(() => logger));
		public static void ConfigureLogger(Lazy<ILoggerAdapter> loggerFactory)
		{
			_logger = loggerFactory;
		}

		public static IObservable<T> Trace<T>(this IObservable<T> source) => source.Trace(_logger.Value);
		public static IObservable<T> Trace<T>(this IObservable<T> source, string name) => source.Trace(_logger.Value, name);

		public static IObservable<T> Trace<T>(this IObservable<T> source, ILoggerAdapter logger) => source.Trace(logger, (++GlobalId).ToString());
		public static IObservable<T> Trace<T>(this IObservable<T> source, ILoggerAdapter logger, string name)
		{
			return new TraceObservable<T>(source, name, logger);
		}
	}
}
