using System.Threading.Tasks;
using SlimMessageBus;
using core.commands;

namespace core.lifeCycle;


public class LifeCycleConsumer : IConsumer<events.LifeCycleEvent>
{
    readonly ICommandProcessor processor;

    public LifeCycleConsumer(ICommandProcessor processor)
    {
        this.processor = processor;
    }

    public Task OnHandle(events.LifeCycleEvent e) => processor.ProcessCommand(e.Name);
}