using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.webSockets
{
	using core.logging;
	using core.pubSub;

	public class SocketConnection
	{
		// key takeaways: segment message. end of message true for last segment. Check if open before sending. Don't send two things at once. If cancelled - stop broadcasting.
		private const int bufferSize = 4 * 1024;
		private readonly ILoggerAdapter Logger;
		private readonly Subject<SocketMessage> MessageSubject;

		private readonly CancellationTokenSource IsAboutToClose = new CancellationTokenSource();
		private readonly CancellationTokenSource AbortEverythingCancel = new CancellationTokenSource();

		public SocketConnection(Subject<SocketMessage> messageSubject, ILoggerAdapter<SocketConnection> logger)
		{
			Logger = logger;
			MessageSubject = messageSubject;
		}

		/// <summary>
		/// This will listen for, and send messages fromt the Subject<SocketMessage> class.
		/// Make sure that is setup to send / receive messages before listen is called.
		/// </summary>
		public async Task<IAsyncDisposable> Listen(WebSocket Socket)
		{
			var toDispose = new MyDispose();
			
			// handle cancelation
			toDispose.Add(() => {
				IsAboutToClose.Cancel();
				AbortEverythingCancel.CancelAfter(TimeSpan.FromSeconds(10));
			});

			var receiveTask = Task.Run(async () => await ReceiveMessages(Socket));
			toDispose.Add(receiveTask); // will execute right away.
			toDispose.Add(() => ClosingSequence(Socket)); // will get executed when disposed.

			// handle listening to inputs.
			toDispose.Add(MessageSubject.Add(m => SendMessage(Socket, m)));

			// after everything else, dispose the socket.
			toDispose.Add(() => AbortEverythingCancel.Cancel());
			toDispose.Add(Socket);

			// make sure this gets run.
			await receiveTask;

			return toDispose;
		}

		private async Task ClosingSequence(WebSocket Socket)
		{
			if(Socket.State != WebSocketState.Closed && Socket.State != WebSocketState.Aborted || AbortEverythingCancel.IsCancellationRequested)
				return;
			await Socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing", AbortEverythingCancel.Token);
		}

		private async Task ReceiveMessages(WebSocket Socket)
		{
			var partialMessage = new StringBuilder();
			var buffer = new ArraySegment<byte>(new byte[bufferSize]);
			while(Socket.State != WebSocketState.Closed && Socket.State != WebSocketState.Aborted)
			{
				if(IsAboutToClose.IsCancellationRequested || AbortEverythingCancel.IsCancellationRequested)
				{
					Logger.LogWarning("Receive loop cancelled.");
					return;
				}
				
				var receive = await Socket.ReceiveAsync(buffer, AbortEverythingCancel.Token);

				if(IsAboutToClose.IsCancellationRequested) // cancellation can happen durring the above command.
					return;

				// closing request received
				if (Socket.State == WebSocketState.CloseReceived && receive.MessageType == WebSocketMessageType.Close)
				{
					Logger.LogWarning($"Socket is closing. Acknowledging Close frame received from client");
					IsAboutToClose.Cancel();
					await Socket.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, "Acknowledge Close frame", CancellationToken.None);
					return; // the socket state changes to closed at this point
				}
				var text = Encoding.UTF8.GetString(buffer.Array, 0, receive.Count);
				partialMessage.Append(text);

				if(receive.EndOfMessage)
				{
					await MessageSubject.Publish(new SocketMessage(partialMessage.ToString()));
					partialMessage.Clear();
				}
			}
			Logger.LogWarning("ended websocket receive loop.");
		}

		// this method will break up the string message in the socket message into multiple sends via the websocket.
		// If cancellation happens, it will simply return for the about to close. Abort cancellation should happen immediately.
		private async Task SendMessage(WebSocket Socket, SocketMessage message)
		{
			try
			{
				var segments = Encoding.UTF8.GetBytes(message.Message)
					.Select((c, index) => new { c, index })
					.GroupBy(x => x.index / bufferSize)
					.Select(g => g.Select(x=>x.c))
					.ToList();

				for(var i = 0; i < segments.Count(); ++i)
				{
					var buffer = new ArraySegment<byte>(segments[i].ToArray());
					var segment = segments[i];
					var isLast = i == segments.Count() - 1;
					if(IsAboutToClose.IsCancellationRequested || AbortEverythingCancel.IsCancellationRequested)
						return;
					await Socket.SendAsync(buffer, WebSocketMessageType.Text, isLast, AbortEverythingCancel.Token);
				}
			}
			catch(Exception e)
			{
				Logger.LogError("Exception sending data to websocket: {eMessage}", e.Message);
			}
		}
	}

	public class MyDispose : IAsyncDisposable
	{
		private readonly List<Func<Task>> toDispose = new List<Func<Task>>();

		public void Add(Func<Task> dispose) => toDispose.Add(dispose);
		public void Add(Task dispose) => Add(() => dispose);
		public void Add(Action dispose) => Add(() => Task.Run(dispose));
		public void Add(IDisposable dispose) => Add(dispose.Dispose);

		public async ValueTask DisposeAsync()
		{
			foreach(var d in toDispose)
				await d();
		}
	}
}
