using System.Text.RegularExpressions;

namespace core.gcode
{
	[Immutability.Immutable]
	public class GCodeParser
	{
		private const string numberParsePart = "\\s*([-+]?\\d+(\\.\\d*)?)";
		private static Regex XReg = new Regex("[xX]" + numberParsePart, RegexOptions.Compiled);
		private static Regex YReg = new Regex("[yY]" + numberParsePart, RegexOptions.Compiled);
		private static Regex ZReg = new Regex("[zZ]" + numberParsePart, RegexOptions.Compiled);
		private static Regex FReg = new Regex("[fF]" + numberParsePart, RegexOptions.Compiled);

		private static float? ExtractValue(Regex r, string input)
		{
			var match = r.Match(input);
			if(match.Success && match.Groups.Count >= 2)
			{
				var capturedText = match.Groups[1].Value;
				if(float.TryParse(capturedText, out var number))
					return number;
			}
			return null;
		}


		public static bool TryParseMove(string input, out float? x, out float? y, out float? z, out float? feedRate)
		{
			feedRate = null;
			if(TryParseMove(input, out x, out y, out z))
			{
				feedRate = ExtractValue(FReg, input);
				return true;
			}
			return false;
		}

		public static bool TryParseMove(string input, out float? x, out float? y, out float? z)
		{
			// everything starts out with being not set (null).
			x = y = z = null;

			if(string.IsNullOrWhiteSpace(input))
				return false;
			// a move can only be g0 or g1 (for now)
			if(!input.StartsWith("G0") && !input.StartsWith("G1"))
				return false;

			// try get the x value
			x = ExtractValue(XReg, input);
			y = ExtractValue(YReg, input);
			z = ExtractValue(ZReg, input);

			return true;
		}
	}
}
