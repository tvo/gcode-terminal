ARG BASE_ARCH=mcr.microsoft.com/dotnet/sdk:8.0
ARG  RUN_ARCH=mcr.microsoft.com/dotnet/aspnet:8.0

FROM ${BASE_ARCH} AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY api/*.csproj ./api/
COPY core/*.csproj ./core/
COPY test/*.csproj ./test/
COPY app/*.csproj ./app/
COPY *.sln ./
RUN dotnet restore

# Copy everything else and build
COPY ./api ./api
COPY ./core ./core
RUN dotnet publish api -c Release -o out

# Build runtime image
FROM ${RUN_ARCH}
EXPOSE 3000/tcp
WORKDIR /api
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "api.dll"]

