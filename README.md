# GCode-Terminal

# Motivation
I am in the long process of building and improving my own cnc machine. It is a LowRider V2, with a ArduinoMega (Marlin) as the controller, with a headless raspberry pi that acts as the primary interface.
I couldn't find any open source software which allowed me to run gcode files straight from the terminal. So I thought I would add to the scope of my already big project and create a program which can send / run gcode from the terminal.

# How to Test
run "dotnet test" in the root directory.

# How to use
todo: create and link to wiki pages.
appsettings.json has the port configuration.
"dotnet run" to start it off
arrow keys to jog the machine
'+' and '-' change the scale of how far the machines travels per jog.

# License
MIT
