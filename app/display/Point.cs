namespace serial.display
{
	[Immutability.Immutable]
	public class Point
	{
		public readonly int X;
		public readonly int Y;
		public Point(int x, int y)
		{
			X = x;
			Y = y;
		}

		public Point OnlyX => new Point(X, 0);
		public Point OnlyY => new Point(0, Y);

		public static Point operator +(Point left, Point right)
		{
			return new Point(left.X + right.X, left.Y + right.Y);
		}

		public (int, int) Deconstruct() => (X, Y);

		public override string ToString() => $"({X}, {Y})";

		public static Point Zero = new Point(0, 0);
		public static Point One = new Point(1, 1);
	}
}
