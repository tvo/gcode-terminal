using System;

namespace serial.display
{
	public class ConsoleScreen : Window, IDraw, IDisposable
	{
		public IDraw t => this;
		public ConsoleScreen()
			: base(Point.Zero, new Square(Console.WindowWidth, Console.WindowHeight))
		{
		}

		public void Init()
		{
			Console.CursorVisible = false;
			Console.Clear();
		}

		public void Dispose()
		{
			Console.WriteLine();
			Console.Clear();
		}

		private object _lock = new Object();
		public override void Draw(string line, Point p)
		{
			var (x, y) = (p).Deconstruct();

			// exit early
			if(x > Width || y > Height)
				return;
			if(x < 0 || y < 0)
				return;

			lock(_lock)
			{
				Console.SetCursorPosition(x, y);
				Console.Write(line);
			}
		}
		private void WriteCharacter(char c, Point p) => Draw(c.ToString(), p);
	}
}
