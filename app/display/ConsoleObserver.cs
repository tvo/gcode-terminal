using System;

namespace serial.display
{
	public class ConsoleObserver : IObserver<string>
	{
		private Action<string> WriteLine;

		public ConsoleObserver(IWindow w)
		{
			WriteLine = input => {
				w.WriteLine(input);
			};
		}

		private string RemoveBad(string value)
		{
			return value
				.Replace("\n", "")
				.Replace("\r", "");
		}

		public void OnNext(string value)
		{
			WriteLine(RemoveBad(value));
		}

		public void OnError(Exception e)
		{
			WriteLine(e.Message);
		}

		public void OnCompleted()
		{
		}
	}
}
