namespace serial.display
{
	public interface IDraw
	{
		void Draw(string content, Point location);
	}
}
