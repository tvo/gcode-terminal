namespace serial.display
{
	public class Square
	{
		public readonly int Width;
		public readonly int Height;

		public Square(int width, int height)
		{
			Width = width;
			Height = height;
		}
	}
}
