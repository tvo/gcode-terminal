using System;
using System.Linq;
using System.Collections.Immutable;

namespace serial.display
{
	public interface IWindow : IDraw
	{
		void WriteLine(string line);
		// void Draw(string content, Point location); // is in IDraw
		void Clear();

		IWindow SplitTop(string title = null, bool border = false);
		IWindow SplitBottom(string title = null, bool border = false);
		IWindow SplitLeft(string title = null, bool border = false);
		IWindow SplitRight(string title = null, bool border = false);
		(IWindow, IWindow) WithStatusBar(string title = null, bool border = false);
	}
}
