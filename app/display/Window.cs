using System;
using System.Linq;
using System.Collections.Immutable;

namespace serial.display
{
	public class Window : IWindow
	{
		public int Height => Square.Height;
		public int Width => Square.Width;

		public readonly Square Square;
		public readonly Point Point;

		private readonly IDraw _draw;

		public ImmutableList<string> Lines = ImmutableList<string>.Empty;

		public Window(Point point, Square square)
		{
			Point = point ?? Point.Zero;
			Square = square;
		}

		public Window(Point point, int w, int h) : this(point, new Square(w, h)) { }
		public Window(IDraw draw, Point point, int w, int h) : this(draw, point, new Square(w, h)) { }

		public Window(IDraw draw, Point point, Square square)
			: this(point, square)
		{
			_draw = draw;
		}

		public virtual void Draw(string content, Point location)
		{
			_draw.Draw(content, Point + location);
		}

		public override string ToString() => $"p:{Point}, w:{Width}, h:{Height}";

		public void WriteLine(string line)
		{
			if(line is null)
				return;
			if(line.Length > Width)
				line = line.Substring(0, Width);
			Lines = Lines.Add(line);
			if(Lines.Count > Height)
			{
				Lines = Lines.RemoveRange(0, 1);
				ReDraw();
				// discard the lines off of the screen. Remove this if scolling.
			}
			else
				Draw(line, new Point(0, Lines.Count - 1));
		}

		private IWindow AddSubWindow(Window w, string title = null, bool border = false)
		{
			var hasTitle = !string.IsNullOrWhiteSpace(title);
			if(!hasTitle && !border)
				return w;

			var p = w.Point;
			var width = w.Width;
			var height = w.Height;

			// draw border and title
			if(border)
			{
				foreach(var (line, point) in w.Border())
					w.Draw(line, point);
			}
			if(hasTitle) // title gets drawn over the border
				w.Draw(title, new Point(2, 0));

			// move the point logic
			if(hasTitle || border) // shift down
			{
				p += Point.One.OnlyY;
				height -= 1; // a border overlaps the title, this if - else if expression should handle that.
			}
			if(border) // right one
			{
				p += Point.One.OnlyX;
				// also border is smaller in height too.
				width -= 2;
				height -= 1; // -1 because if title has already moved it down one.
			}

			// fin.
			return new Window(this, p, width, height);
		}

		public IWindow SplitTop(string title = null, bool border = false) => AddSubWindow(new Window(this, 	Point.Zero, 				Width, Height / 2), title, border);
		public IWindow SplitBottom(string title = null, bool border = false) => AddSubWindow(new Window(this, new Point(0, Height / 2), 	Width, Height / 2), title, border);
		public IWindow SplitLeft(string title = null, bool border = false) => AddSubWindow(new Window(this, 	Point.Zero, 				Width / 2, Height), title, border);
		public IWindow SplitRight(string title = null, bool border = false) => AddSubWindow(new Window(this, new Point(Width / 2, 0), 	Width / 2, Height), title, border);

		public (IWindow, IWindow) WithStatusBar(string title = null, bool border = false)
		{
			var main = AddSubWindow(new Window(this, Point.Zero, Width, Height - 1));
			var b = new Point(0, Height - 1);
			var status = AddSubWindow(new Window(this, b, Width, 1), null, false);
			return (main, status);
		}

		private ImmutableList<Tuple<string, Point>> Border()
		{
			var result = ImmutableList<Tuple<string, Point>>.Empty;
			var special = "─│┌┐└┘";
			var topBottomShared = new string(special[0], Width - 2);
			void Add(string s, Point p) => result = result.Add(Tuple.Create(s, p));

			// top
			Add(special[2] + topBottomShared + special[3], Point.Zero);
			// sides
			foreach(var i in Enumerable.Range(1, Height - 2))
			{
				var p = new Point(0, i);
				Add(special[1].ToString(), p);
				p += new Point(Width - 1, 0);
				Add(special[1].ToString(), p);
			}
			// bottom
			Add(special[4] + topBottomShared + special[5], new Point(0, Height - 1));
			return result;
		}

		public void Clear()
		{
			var blank = new string(' ', Width - 1);
			var p = Point.Zero;
			for(var i = 0; i < Height; ++i)
			{
				Draw(blank, p);
				p += new Point(0, 1);
			}
		}

		private void ReDraw()
		{
			// clear
			Clear();
			// redraw line by line
			var p = Point.Zero;
			foreach(var line in Lines)
			{
				Draw(line, p);
				p = p + new Point(0, 1);
			}
		}
	}
}
