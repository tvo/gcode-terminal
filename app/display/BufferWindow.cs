using System;

namespace serial.display
{
	public class BufferWindow : IWindow
	{
		private readonly TimeSpan time;
		private readonly IWindow instance;

		public BufferWindow(TimeSpan time, IWindow instance)
		{
			this.time = time;
			this.instance = instance;
		}

		public void Draw(string content, Point location) => instance.Draw(content, location);
		public override string ToString() => instance.ToString();
		public void WriteLine(string line) => instance.WriteLine(line);

		public IWindow SplitTop(string title = null, bool border = false) => instance.SplitTop(title, border);
		public IWindow SplitBottom(string title = null, bool border = false) => instance.SplitBottom(title, border); 
		public IWindow SplitLeft(string title = null, bool border = false) => instance.SplitLeft(title, border);
		public IWindow SplitRight(string title = null, bool border = false) => instance.SplitRight(title, border);

		public (IWindow, IWindow) WithStatusBar(string title = null, bool border = false) => instance.WithStatusBar(title, border);

		public void Clear() => instance.Clear();
	}
}
