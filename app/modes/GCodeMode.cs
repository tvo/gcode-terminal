using System;
using System.Threading.Tasks;

namespace serial.modes
{
	using core.io;

	public class GCodeMode : BaseEditMode
	{
		private readonly ISerialWriter Writer;

		public GCodeMode(ISerialWriter writer)
			: base("=>", k => k == "g")
		{
			Writer = writer;
		}

		protected override async Task<string> Execute(string text)
		{
			await Writer.Write(text);
			return text;
		}
	}
}
