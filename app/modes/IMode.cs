using System;
using System.Threading.Tasks;

namespace serial.modes
{
	public interface IMode
	{
		Task<bool> Process(ConsoleKeyInfo shortcut, IObservable<ConsoleKeyInfo> keyInput);
	}
}
