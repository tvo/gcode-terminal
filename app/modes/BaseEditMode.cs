using System;
using System.Threading.Tasks;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace serial.modes
{
	using core.rx;

	public abstract class BaseEditMode : IMode
	{
		protected readonly string Banner;
		protected readonly Func<string, bool> isMe;

		public IObservable<string> Output => Done;

		protected string Wip = "";

		private readonly Subject<string> Done = new Subject<string>();

		public BaseEditMode(string banner, Func<string, bool> meFunc)
		{
			Banner = banner;
			isMe = meFunc;
		}

		protected abstract Task<string> Execute(string command);

		private string ApplyChar(ConsoleKeyInfo key, string str)
		{
			return str + key.KeyChar.ToString();
		}

		private string ApplyBackspace(ConsoleKeyInfo key, string str)
		{
			if(key.Key != ConsoleKey.Backspace)
				return str;
			if(str.Length <= 1)
				return "";

			return str.Substring(0, str.Length - 2);
		}

		public async Task<bool> Process(ConsoleKeyInfo key, IObservable<ConsoleKeyInfo> keyInput)
		{
			var result = isMe(key.KeyChar.ToString());
			if(!result)
				return false;
			Done.OnNext(Banner);
			var input = "";
			await keyInput.TakeUntil(x => x.Key == ConsoleKey.Enter)
				.Select(x => Tuple.Create(ApplyChar(x, input), x))
				.Select(x => ApplyBackspace(x.Item2, x.Item1))
				.Tap(x => Done.OnNext(Banner + x))
				.Tap(x => input = x)
				.LastAsync(); // last to to let await block until finished.

			if(string.IsNullOrEmpty(input))
				Done.OnNext("");
			else
				Done.OnNext(await Execute(input.Trim()));

			Wip = "";
			return true;
		}
	}
}
