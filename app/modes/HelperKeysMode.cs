using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace serial.modes
{
	using core.io;

	public class HelperKeysMode : IMode
	{
		private readonly Func<string, Task> Write;
		private IDictionary<string, string> KeyToCode = new Dictionary<string, string>();

		public HelperKeysMode(ScaleMode scaleMode, ISerialWriter writer)
		{
			var started = false;
			Write = async x => {
				if(started)
					await writer.Write(x);
			};

			KeyToCode.Add("H", "G92 X0 Y0 Z0");
		}

		public async Task<bool> Process(ConsoleKeyInfo key, IObservable<ConsoleKeyInfo> input)
		{
			var k = key.KeyChar.ToString();
			if(!KeyToCode.ContainsKey(k)) // caps?
				return false;
			await Write(KeyToCode[k]);
			return true;
		}
	}
}
