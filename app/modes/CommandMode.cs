using System.Threading.Tasks;

namespace serial.modes
{
	using commands;
	using core.commands;

	public class CommandMode : BaseEditMode
	{
		private readonly ICommandProcessor Processor;

		public CommandMode(ICommandProcessor processor)
			: base(":", k => k == ":")
		{
			Processor = processor;
		}

		protected override Task<string> Execute(string command)
		{
			return Processor.ProcessCommand(command);
		}
	}
}
