using System;
using System.Threading.Tasks;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace serial.modes
{
	public class ScaleMode : IMode
	{

		private float scale => Scale.Value;
		const float scaleFactor = 2;
		public readonly BehaviorSubject<float> Scale = new BehaviorSubject<float>(10);
		public IObservable<string> Output => Scale.Select(x => x.ToString());

		private const string ZoomIn = "+=";
		private const string ZoomOut = "-_";
		private static readonly string ZoomCommands = ZoomIn + ZoomOut;

		public Task<bool> Process(ConsoleKeyInfo key, IObservable<ConsoleKeyInfo> input)
		{
			var c = key.KeyChar;
			if(!ZoomCommands.Contains(c))
				return Task.FromResult(false);

			if(ZoomIn.Contains(c))
				Scale.OnNext(scale * scaleFactor);
			else
				Scale.OnNext(scale / scaleFactor);
			return Task.FromResult(true);
		}
	}
}
