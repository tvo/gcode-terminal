using System;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using SlimMessageBus;

namespace serial.modes
{
	using core.io;

	public class JogMode : IMode, IConsumer<LifeCycleEvent>
	{
		private readonly Func<float> ScaleFunc;
		private readonly Func<string, Task> Write;

		private bool started = false;
		public JogMode(ScaleMode scaleMode, ISerialWriter writer)
		{
			ScaleFunc = () => scaleMode.Scale.Value;

			Write = async x => {
				if(started)
					await writer.Write(x);
			};
		}

		public readonly Subject<string> ScaleCode = new Subject<string>();
		public IObservable<string> Output => ScaleCode;

		public Task OnHandle(LifeCycleEvent e)
		{
			if(!started && e.Name == "system-startup")
				started = true;
			return Task.CompletedTask;
		}

		private string MoveInput(ConsoleKeyInfo cInfo)
		{
			var scale = ScaleFunc();
			if(cInfo.Key == ConsoleKey.UpArrow)
				return "Y" + scale;
			else if(cInfo.Key == ConsoleKey.DownArrow)
				return "Y" + (scale * -1);
			else if(cInfo.Key == ConsoleKey.RightArrow)
				return "X" + scale;
			else if(cInfo.Key == ConsoleKey.LeftArrow)
				return "X" + (scale * -1);
			else if(cInfo.KeyChar == ',')
				return "Z" + scale;
			else if(cInfo.KeyChar == '.')
				return "Z" + (scale * -1);
			return null;
		}

		public async Task<bool> Process(ConsoleKeyInfo key, IObservable<ConsoleKeyInfo> keys)
		{
			var gMove = MoveInput(key);
			if(gMove == null)
				return false;
			gMove = "G0 " + gMove;
			ScaleCode.OnNext(gMove);
			await Write(gMove);
			return true;
		}
	}
}
