using System.Threading.Tasks;

namespace serial.Http
{
	public interface IHttp
	{
		Task<IResponse> Get(string url);
		Task<IResponse> Post(string url, string body);
		Task<IResponse> Post(string url, string body, string mediaType);
	}
}
