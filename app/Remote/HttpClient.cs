using System.Threading.Tasks;
using System.Net.Http;
using System.Text;

namespace serial.Http
{
	public class HttpClient : IHttp
	{
		private System.Net.Http.HttpClient GetClient()
		{
			return new System.Net.Http.HttpClient();
		}

		public async Task<IResponse> Get(string url)
		{
			using var client = GetClient();
			var httpResponse = await client.GetAsync(url);
			return new Response((int)httpResponse.StatusCode, await httpResponse.Content.ReadAsStringAsync());
		}

		public async Task<IResponse> Post(string url, string body)
		{
			using var client = GetClient();
			var httpResponse = await client.PostAsync(url, new StringContent(body));
			return new Response((int)httpResponse.StatusCode, await httpResponse.Content.ReadAsStringAsync());
		}

		public async Task<IResponse> Post(string url, string body, string mediaType)
		{
			using var client = GetClient();
			var httpResponse = await client.PostAsync(url, new StringContent(body, Encoding.UTF8, mediaType));
			return new Response((int)httpResponse.StatusCode, await httpResponse.Content.ReadAsStringAsync());
		}
	}
}
