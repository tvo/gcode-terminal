namespace serial.Http
{
	public interface IResponse
	{
		string Body { get; }
		int StatusCode { get; }
		bool IsSuccess { get; }
		T As<T>();
	}
}
