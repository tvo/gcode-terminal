namespace serial.Http
{
	[Immutability.Immutable]
	public class Response : IResponse
	{
		public string Body { get; }
		public int StatusCode { get; }
		public bool IsSuccess => StatusCode >= 200 && StatusCode < 300;

		public Response(int statusCode, string response)
		{
			StatusCode = statusCode;
			Body = response;
		}

		public T As<T>()
		{
			return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(Body);
		}
	}
}
