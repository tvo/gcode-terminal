using System.Threading.Tasks;

namespace serial.Http
{
	using core.logging;

	public class HttpClientLogger : IHttp
	{
		private readonly IHttp Instance;
		private readonly ILoggerAdapter Logger;

		public HttpClientLogger(IHttp instance, ILoggerAdapter<HttpClientLogger> logger)
		{
			Instance = instance;
			Logger = logger;
		}

		public async Task<IResponse> Get(string url)
		{
			Logger.LogDebug("Http: fetching get request for url: {url}", url);
			var response = await Instance.Get(url);
			Logger.LogDebug("Http: request from {url} => {statuscode} - with body: {body}", url, response.StatusCode, response.Body);
			return response;
		}

		public async Task<IResponse> Post(string url, string body)
		{
			//Logger.LogDebug("Http: Posting to {url}", url);
			var response = await Instance.Post(url, body);
			Logger.LogDebug("Http: Posted to {url} => {status}", url, response.StatusCode);
			return response;
		}

		public async Task<IResponse> Post(string url, string body, string mediaType)
		{
			//Logger.LogDebug("Http: Posting to {url}", url);
			var response = await Instance.Post(url, body, mediaType);
			Logger.LogDebug("Http: Posted to {url} => {status}", url, response.StatusCode);
			return response;
		}
	}
}
