using SlimMessageBus;

namespace serial;

using core.io;
using core.logging;
using Http;

public class RemoteWriter : ISerialWriter
{
	private readonly IHttp Http;
	private readonly string Server;
	private readonly IMessageBus bus;
	private readonly ILoggerAdapter<RemoteWriter> logger;

	public RemoteWriter(IHttp http, IConfiguration config, IMessageBus bus, ILoggerAdapter<RemoteWriter> logger)
	{
		Http = http;
		Server = config["server"];
		this.bus = bus;
		this.logger = logger;
	}

	public async Task Write(string line)
	{
		if(string.IsNullOrWhiteSpace(line))
			return;
		await bus.Publish(new GCodeWriteEvent(line));
		await Http.Post($"{Server}/GCode", line, "text/plain");
	}

	public async Task Write(IEnumerable<string> lines)
	{
		foreach(var line in lines.Where(x => !string.IsNullOrWhiteSpace(x)))
			await Write(line);
	}

	public void StartHandle() { } // The remote reader is always ready.
}
