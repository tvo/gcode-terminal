using System.Threading;
using System.Net.WebSockets;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;

namespace serial
{
	using core.io;
	using core.rx;
	using core.logging;
	using core.webSockets;

	public class RemoteReader : ISerialReader
	{
		private readonly core.pubSub.Subject<SocketMessage> SocketSubject;

		private readonly ILoggerAdapter Logger;
		private readonly IObservable<string> listen;

		public RemoteReader(core.pubSub.Subject<SocketMessage> socketSubject,
							SocketConnection connection,
							IConfiguration config,
							ILoggerAdapter<RemoteReader> logger)
		{
			Logger = logger;
			SocketSubject = socketSubject;

			listen = StartListening(config["server"], connection)
				.Broadcast()
				//.ObserveOn(DefaultScheduler.Instance)
				//.SubscribeOn(DefaultScheduler.Instance)
				;
		}

		public IObservable<string> Listen() => listen.Do(line => EvaluateStartup(line));

		private IObservable<string> StartListening(string server, SocketConnection connection)
		{
			var url = $"{server}/Micro/console".Replace("https", "wss").Replace("http", "ws");
			var client = new ClientWebSocket();
			var close = new CancellationTokenSource();

			Logger.LogWarning("....... Server url for websocket: {socketUrl}", url);
			
			return Observable.Create<string>(async (IObserver<string> observer) => {
				await client.ConnectAsync(new Uri(url), close.Token);

				var toDispose = new CompositeDisposable();
				toDispose.Add(SocketSubject.Add(message => {
					Logger.LogDebug("Got message from websocket: {message}", message.Message);
					observer.OnNext(message.Message);
				}));
				var listenDispose = await connection.Listen(client);
				toDispose.Add(Disposable.Create(() => listenDispose.DisposeAsync().AsTask().Wait()));

				return toDispose;
			}).Catch((Exception e) => {
				Logger.LogError("Websocket exception, reason: {exceptionMessage}", e.Message);
				Logger.LogError("Exception tostring, {msg}", e.ToString());
				client.CloseAsync(WebSocketCloseStatus.NormalClosure, null, default).Wait();
				Logger.LogError("finished closing");
				return Observable.Empty<string>();
			});
		}

		private bool started = false;
		public bool Started() => started;
		private bool EvaluateStartup(string line)
		{
			if(!started && line.ToLower().Contains("free memory"))
				started = true;
			return started;
		}
	}
}
