using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace serial.io
{
	using core.io;

	public class SerialWriterToDisplay : ISerialWriter
	{
		private readonly ISerialWriter Instance;
		private readonly Lazy<IMainUi> Main;

		public SerialWriterToDisplay(ISerialWriter instance, Lazy<IMainUi> main)
		{
			Instance = instance;
			Main = main;
		}

        public async Task Write(string data)
        {
			await Instance.Write(data);
			Main.Value.AddGcodeSent(data);
        }

        public async Task Write(IEnumerable<string> lines)
        {
			lines = lines.Select(x => {
				Main.Value.AddGcodeSent(x);
				return x;
			});
			await Instance.Write(lines);
        }

		public void StartHandle() => Instance.StartHandle();
    }
}
