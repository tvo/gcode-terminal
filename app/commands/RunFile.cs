using System;
using System.IO;
using System.Threading.Tasks;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using SlimMessageBus;

namespace serial.commands
{
	using core.logging;
	using core.io;
	using core.commands;

	[Immutability.Immutable]
	public class RunFile : ICommand
	{
		public string Name { get; } = "run";
		private readonly ISerialWriter Writer;
		private readonly IObservable<char> keyObservable;
		private readonly IMessageBus bus;
		private readonly ILoggerAdapter logger;

		public RunFile(ISerialWriter writer, KeyObservable keyObservable, IMessageBus bus, ILoggerAdapter<RunFile> logger)
		{
			Writer = writer;
			this.keyObservable = keyObservable.KeysPressed.Select(x => x.KeyChar);
			this.logger = logger;
			this.bus = bus;
		}

		public bool IsMe(string arg) => arg.StartsWith("run ");
		public async Task<string> DoCommand(string[] args)
		{
			if(args.Length != 1)
				return "Bad number of arguments. {" + string.Join(" ", args) + "}";
			var arg = args[0];

			if(!File.Exists(arg))
				return "File not found: " + arg;

			await bus.Publish(new core.events.LifeCycleEvent("prejob"));

			var keySubject = new BehaviorSubject<char>('@');
			var keySub = keyObservable.Subscribe(keySubject);
			foreach(var line in await File.ReadAllLinesAsync(arg))
			{
				logger.LogInformation("read line: {line}", line);
				
				if(keySubject.Value == ' ')
				{
					logger.LogInformation("yo yo yo, you pressed the spacebar. this should pause / stop the gcode for a bit.");
					await keySubject.Skip(1).Where(x => x == ' ').Take(1);
					// so that it doesn't get paused for the next gcode line.
					keySubject.OnNext('@');
				}

				await Writer.Write(line);
			}

			await bus.Publish(new core.events.LifeCycleEvent("postjob"));
			keySub.Dispose();

			return ":run " + arg;
		}
	}
}
