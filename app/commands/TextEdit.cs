using System;
using System.Text.RegularExpressions;

namespace serial
{
	public class TextEdit
	{
		private readonly string Line;

		public TextEdit(string line)
		{
			Line = line;
		}

		private static readonly Regex AlphaNumeric = new Regex("[/\\w\\d \\.?!]", RegexOptions.Compiled);

		/// <summary> This will return string with the processed string.
		public string Process(ConsoleKeyInfo key)
		{
			var result = Line;
			var c = key.KeyChar.ToString();

			if(AlphaNumeric.IsMatch(c))
				return Line + c;
			else
			{
				if(key.Key == ConsoleKey.Escape)
					return null;
				if(key.Key == ConsoleKey.Backspace)
				{
					if(string.IsNullOrEmpty(Line))
						return null;
					return Line.Substring(0, Line.Length - 1);
				}
			}

			return Line;
		}
	}
}
