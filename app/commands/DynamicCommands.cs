using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace serial.commands
{
	using Http;
	using core.commands;

	public class DynamicCommands
	{
		private readonly List<ICommand> dynamicCommands = new List<ICommand>();
		private readonly IHttp Http;
		private readonly ICommandProcessor Processor;
		private readonly string Server;
		private readonly Func<string, Func<Task<string>>, DynamicCommand> factory;

		public DynamicCommands(IHttp http, ICommandProcessor processor, IConfiguration config,
							   Func<string, Func<Task<string>>, DynamicCommand> dynCommandFactory)
		{
			factory = dynCommandFactory;
			Http = http;
			Processor = processor;
			Server = config["server"];
		}
		
		private async Task<string> DoRemote(string commandName)
		{
			var r = await Http.Post($"{Server}/command/{commandName}", "");
			return r.Body;
		}

		public async Task GetRemoteCommands()
		{
			var response = await Http.Get($"{Server}/command");
			var commands = response.As<List<string>>();
			foreach(var commandName in commands)
			{
				var dyn = factory(commandName, () => DoRemote(commandName));
				Processor.AddCommand(dyn);
			}
		}
	}
}
