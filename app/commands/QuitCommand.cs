using System.Threading.Tasks;

namespace serial.commands
{
	using Events;
	using core.pubSub;
	using core.commands;

	public class QuitCommand : ICommand
	{
		public string Name { get; } = "quit";
		private readonly Subject<Quit> quitSubject;

		public QuitCommand(Subject<Quit> quit)
		{
			quitSubject = quit;
		}

		public bool IsMe(string arg) => arg.Contains("quit ");
		public Task<string> DoCommand(string[] args)
		{
			quitSubject.Publish(Quit.Instance);
			return Task.FromResult("exiting...");
		}
	}
}
