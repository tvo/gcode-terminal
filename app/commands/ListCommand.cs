using System;
using System.Threading.Tasks;

namespace serial.commands
{
	using core.commands;

	[Immutability.Immutable]
	public class ListCommand : ICommand
	{
		public string Name { get; } = "list";
		private readonly Func<ICommandProcessor> processorFactory;

		public ListCommand(Func<ICommandProcessor> processorFactory)
		{
			this.processorFactory = processorFactory;
		}

		public bool IsMe(string arg) => arg.StartsWith(Name + " ");
		public Task<string> DoCommand(string[] args)
		{
			var processor = processorFactory();
			return Task.FromResult(string.Join(", ", processor.AllCommands.Keys));
		}
	}
}
