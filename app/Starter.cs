using Autofac;
using System.Reactive.Disposables;

namespace serial
{
	using core.logging;

	public class Starter : IStartable, IDisposable
	{
		private readonly ILoggerAdapter logger;

		public Starter(ILoggerAdapter<Starter> logger)
		{
			this.logger = logger;
		}

		void IStartable.Start()
		{
			logger.LogDebug("starting up.");
		}

		public void Dispose()
		{
			logger.LogDebug("shutting down.");
		}
	}
}
