using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using SlimMessageBus.Host;
using SlimMessageBus.Host.Memory;
using System.Reflection;

namespace serial
{
	using Http;
	using modes;
	using commands;

	public class MainModule : Autofac.Module
	{
		private readonly IConfiguration config;

		public MainModule(IConfiguration config)
		{
			this.config = config;
		}

		private void RegisterClientOnlyTypes(ContainerBuilder builder)
		{
			if(!config.GetValue<bool>("useServer", false))
				return;

			builder.RegisterType<RemoteWriter>().AsImplementedInterfaces().SingleInstance();
			builder.RegisterType<RemoteReader>().AsImplementedInterfaces().SingleInstance();
		}

		private ServiceCollection MicrosoftDI()
		{
			var services = new ServiceCollection();
			services.AddSlimMessageBus(mbb =>
			{
			  mbb.AddServicesFromAssembly(Assembly.GetExecutingAssembly());
			});
			return services;
		}

		protected override void Load(ContainerBuilder builder)
		{
			builder.Populate(MicrosoftDI());

			// high level singletons
			builder.RegisterType<Program>().SingleInstance();
			builder.RegisterType<ModeMultiplexer>().InstancePerLifetimeScope();
			builder.RegisterType<MainUi>().As<IMainUi>().InstancePerLifetimeScope();

			// Life cycle
			builder.RegisterType<KeyObservable>().SingleInstance();
			builder.RegisterType<ModeMultiplexer>().AsImplementedInterfaces();

			// display
			builder.RegisterType<display.ConsoleScreen>();

			// modes.
			builder.RegisterType<ScaleMode>().AsSelf().As<IMode>().InstancePerLifetimeScope();
			builder.RegisterType<JogMode>().AsSelf().As<IMode>().InstancePerLifetimeScope();
			builder.RegisterType<CommandMode>().AsSelf().As<IMode>();
			builder.RegisterType<HelperKeysMode>().AsSelf().As<IMode>();
			builder.RegisterType<GCodeMode>().AsSelf().As<IMode>();

			// commands
			builder.RegisterType<RunFile>().AsImplementedInterfaces().InstancePerDependency();
			builder.RegisterType<ListCommand>().AsImplementedInterfaces().InstancePerLifetimeScope();
			builder.RegisterType<QuitCommand>().AsImplementedInterfaces().InstancePerLifetimeScope();
			builder.RegisterType<DynamicCommands>().AsSelf();
			builder.RegisterType<DynamicCommand>().AsSelf();

			builder.RegisterType<HttpClient>().As<IHttp>();
			builder.RegisterDecorator<HttpClientLogger, IHttp>();

			builder.RegisterType<Starter>().AsImplementedInterfaces().AutoActivate().SingleInstance();

			RegisterClientOnlyTypes(builder);
		}
	}
}
