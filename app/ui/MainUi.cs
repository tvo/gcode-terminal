using System.Reactive.Linq;
using System.Reactive.Disposables;
using System.Reactive.Concurrency;

namespace serial
{
	using display;
	using modes;
	using core.logging;
	using core.rx;

	// This handles the different "modes" and splits the outputs to different screen sections.
	// Ie, pipes the outputs to the correct window panes.
	public class MainUi : IMainUi, IDisposable
	{
		private readonly ModeMultiplexer Multiplexer;
		private readonly ILoggerAdapter logger;
		private readonly ConsoleObserver left, right, status;
		private readonly CompositeDisposable toDispose = new CompositeDisposable();

		public MainUi(ModeMultiplexer modeMultiplexer, ConsoleScreen window, ILoggerAdapter<MainUi> logger)
		{
			this.logger = logger;
			Multiplexer = modeMultiplexer;

			// setup screen
			window.Init();
			var (t, s) = window.WithStatusBar();

			left = new ConsoleObserver(t.SplitLeft("GCode", true));
			right = new ConsoleObserver(t.SplitRight("serial", true));
			status = new ConsoleObserver(s);
		}

		public void StartupCompleted()
		{
			logger.LogInformation("finishing startup for main ui");
			status.OnNext("");
		}

        public void AddGcodeSent(string data) => left.OnNext(data);
        public void AddGcodeSent(IEnumerable<string> lines)
		{
			foreach(var line in lines)
				AddGcodeSent(line);
		}
		public void AddTtyOutput(string output) => right.OnNext(output);
		
		public void Dispose() => toDispose.Dispose();

		public IDisposable Subscribe()
		{
			// get all the modes
			var disp = new CompositeDisposable();
			disp.Add(Multiplexer.CurrentMode()
					.Where(x => x != null)
					.Select(x => x.GetType().Name).Subscribe());

			IObservable<string> toLeft = Observable.Never<string>();
			IObservable<string> toRight = Observable.Never<string>();
			IObservable<string> toStatus = Observable.Never<string>();

			void merge(ref IObservable<string> s, IObservable<string> also)
			{
				s = s.Merge(also);
			}

			// some of this should be moved to mode multipler and have named observables.
			foreach(var mode in Multiplexer.Modes)
			{
				switch(mode)
				{
					case GCodeMode code:
						merge(ref toStatus, code.Output);
						break;
					case ScaleMode scale:
						merge(ref toLeft, scale.Output.Select(x => "Scale: " + x));
						break;
					case CommandMode command:
						merge(ref toStatus, command.Output);
						break;
				}
			}

			disp.Add(toLeft.Subscribe(left));
			disp.Add(toRight.Subscribe(right));
			disp.Add(toStatus.Subscribe(status));

			status.OnNext("waiting...");
			return disp;
		}
    }
}
