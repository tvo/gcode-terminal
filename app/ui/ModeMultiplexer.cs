using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Reactive.Concurrency;

namespace serial
{
	using core.rx;
	using modes;

	// this handles splitting the modes to different sub sections.
	// ie: commands will get routed to the command processor.
	//     the keyinput will get routed to the correct mode, and switch modes.
	public class ModeMultiplexer
	{
		public IEnumerable<IMode> Modes = new List<IMode>();
		private readonly IObservable<ConsoleKeyInfo> KeyInput;

		public ModeMultiplexer(KeyObservable keys, IEnumerable<IMode> modes)
		{
			Modes = modes.ToList();
			KeyInput = keys.KeysPressed;
		}

		public IObservable<IMode> CurrentMode()
		{
			IMode current = null;
			var processing = false;
			async Task<IMode> SwitchMode(ConsoleKeyInfo key)
			{
				processing = true;
				foreach(var mode in Modes)
				{
					if(await mode.Process(key, KeyInput))
						break;
				}
				processing = false;
				return current;
			}
			return KeyInput.ObserveOn(DefaultScheduler.Instance).Where(x => !processing).Select(x => SwitchMode(x)).Switch();
		}
	}
}
