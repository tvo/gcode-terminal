using SlimMessageBus;

namespace serial;

public class GCodeWriteEventDisplayHandler : IConsumer<GCodeWriteEvent>, IConsumer<GCodeCompletedEvent>, IConsumer<LifeCycleEvent>
{
	private readonly IMainUi mainUi;
	private readonly ILoggerAdapter<GCodeWriteEventDisplayHandler> logger;

	public GCodeWriteEventDisplayHandler(IMainUi mainUi, ILoggerAdapter<GCodeWriteEventDisplayHandler> logger)
	{
		this.mainUi = mainUi;
		this.logger = logger;
	}

	public Task OnHandle(GCodeWriteEvent evnt)
	{
		mainUi.AddGcodeSent(evnt.Line);
		return Task.CompletedTask;
	}

	public Task OnHandle(GCodeCompletedEvent evnt)
	{
		mainUi.AddTtyOutput(evnt.Line);
		return Task.CompletedTask;
	}

	public Task OnHandle(LifeCycleEvent evnt)
	{
		if(evnt.Name == "system-startup")
			mainUi.StartupCompleted();
		return Task.CompletedTask;
	}
}
