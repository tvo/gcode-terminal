using System;
using System.Collections.Generic;

namespace serial
{
	public interface IMainUi
	{
		IDisposable Subscribe(); // basically the run method.
		void StartupCompleted();
		void AddTtyOutput(string output);
		void AddGcodeSent(string data);
		void AddGcodeSent(IEnumerable<string> data);
	}
}
