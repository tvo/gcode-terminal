using System;

namespace serial
{
	[Immutability.Immutable]
	public struct Position
	{
		public readonly float X, Y, Z;

		public Position(float x, float y, float z)
		{
			X = x;
			Y = y;
			Z = z;
		}

		public Position RelativeMove(float? x, float? y, float? z)
		{
			return this + new Position(x ?? 0, y ?? 0, z ?? 0);
		}

		public Position AbsoluteMove(float? x, float? y, float? z)
		{
			return new Position(x ?? X, y ?? Y, z ?? Z);
		}

		private static Position _apply(Position left, Position right, Func<float, float, float> op)
		{
			var x = op(left.X, right.X);
			var y = op(left.Y, right.Y);
			var z = op(left.Z, right.Z);
			return new Position(x, y, z);
		}

		public static Position operator +(Position left, Position right) => _apply(left, right, (le, ri) => le + ri);
		public static Position operator -(Position left, Position right) => _apply(left, right, (le, ri) => le - ri);
		public static Position operator *(Position left, Position right) => _apply(left, right, (le, ri) => le * ri);
		public static Position operator /(Position left, Position right) => _apply(left, right, (le, ri) => le / ri);

		public static Position operator +(Position left, float n) => left + Position.From(n);
		public static Position operator -(Position left, float n) => left - Position.From(n);
		public static Position operator *(Position left, float n) => left * Position.From(n);
		public static Position operator /(Position left, float n) => left / Position.From(n);

		public static Position From(float n) => new Position(n, n, n);
		public static Position Zero = new Position(0, 0, 0);
		public static Position One = new Position(1, 1, 1);
	}
}
