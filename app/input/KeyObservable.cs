using System;
using System.Linq;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Threading;

namespace serial
{
	using core.rx;

	[Immutability.Immutable]
	public class KeyObservable
	{
		public readonly IObservable<ConsoleKeyInfo> KeysPressed;

		public KeyObservable()
		{
			KeysPressed = Observable.Interval(TimeSpan.FromMilliseconds(100))
				.Select(t => {
						return Observable.Concat(ReadKeys().Select(x => Observable.Return(x)));
						})
			.Switch()
			.ObserveOn(ThreadPoolScheduler.Instance)
			.SubscribeOn(ThreadPoolScheduler.Instance)
			.Broadcast()
			;
		}

		private IEnumerable<ConsoleKeyInfo> ReadKeys()
		{
			var chars = new List<ConsoleKeyInfo>(Console.KeyAvailable ? 3 : 0);
			while(Console.KeyAvailable)
			{
				chars.Add(Console.ReadKey(true));
			}
			return chars;
		}
	}
}
