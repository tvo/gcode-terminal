﻿using System.Reactive.Linq;
using Autofac;
using serial.Events;
using core.io;
using core.pubSub;
using core.webSockets;
using System.Reactive.Concurrency;
using SlimMessageBus;

namespace serial;

public class Program
{
	static IConfiguration CreateConfiguration()
	{
		var configBuilder = new ConfigurationBuilder();
		configBuilder.AddJsonFile("appsettings.json");
		configBuilder.AddEnvironmentVariables();
		return configBuilder.Build();
	}

	static async Task Main(string[] args)
	{
		var config = CreateConfiguration();

		// eventually this will be init from a cli argument parser package.
		var appSettings = new AppSettings();
		config.Bind(appSettings);

		var builder = new ContainerBuilder();
		builder.RegisterInstance(config);
		builder.RegisterInstance(appSettings);
		builder.RegisterModule(new MainModule(config));
		builder.RegisterModule(new WebSocketModule(config));
		builder.RegisterModule(new LoggerModule(config));
		builder.RegisterModule(new PubSubModule(config));
		builder.RegisterModule(new IOModule(config));
		builder.RegisterModule(new core.commands.CommandModule(config));
		var provider = builder.Build();

		var useServer = config.GetValue<bool>("useServer", false);
		using var scope = provider.BeginLifetimeScope();
		if(useServer)
			await scope.Resolve<commands.DynamicCommands>().GetRemoteCommands();
		var program = provider.Resolve<Program>();

		// todo - hangfire for non ui threads?
		await program.Start();

		// notifies all listeners to quit.
		await scope.Resolve<Subject<Quit>>().Publish(Quit.Instance);
		System.AppDomain.CurrentDomain.ProcessExit += (sender, _args) => {
			scope.Resolve<Subject<Quit>>().Publish(Quit.Instance).Wait();
		};
	}

	private readonly IMainUi MainUi;
	private readonly IMessageBus bus;
	private readonly ISerialWriter writer;
	private readonly ISerialReader reader;
	private readonly ILoggerAdapter logger;


	public Program(IMainUi mainUi, IMessageBus bus, ISerialWriter writer, ISerialReader reader, AppSettings settings, ILoggerAdapter<Program> logger)
	{
		foreach(var i in Enumerable.Range(1, 4))
			logger.LogTrace("."); // provides a space between runs for reading the file log.
		MainUi = mainUi;
		this.bus = bus;
		this.writer = writer;
		this.reader = reader;
		this.logger = logger;
	}

	public async Task Start()
	{
		var listenerSubject = new System.Reactive.Subjects.Subject<string>();
		using var uiDispose = MainUi.Subscribe();
		using var listenDispose = reader.Listen().Subscribe(listenerSubject);
		writer.StartHandle();

		while(true)
			await Task.Delay(10000); // wait forever. This needs to be here.
	}
}
